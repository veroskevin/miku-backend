syntax = "proto3";

import "google/protobuf/timestamp.proto";

package proto;

message ArticleRequest {
    string id = 1;
    string title = 2;
    string body = 3;
    string slug = 4;
    string status = 5;
    string user_id = 6;
    string category_slug = 7;
    string image_url = 8;
    int32 first = 9;
    string after = 10;
}

message RelatedArticleRequest {
    string id = 1;
    string title = 2;
    string body = 3;
    string slug = 4;
    string status = 5;
    string user_id = 6;
    repeated string category_slug = 7;
    string image_url = 8;
    int32 first = 9;
    string after = 10;
}

message ArticleResponse {
    string id = 1;
    string title = 2;
    string body = 3;
    string slug = 4;
    string status = 5;
    string user_id = 6;
    google.protobuf.Timestamp created_at = 7;
    google.protobuf.Timestamp updated_at = 8;
    repeated CategoryResponse categories = 9;
    string image_url = 10;
}

message ListArticle {
    repeated ArticleResponse data = 1;
}

message CategoryRequest {
    string name = 1;
    string slug = 2;
    string user_id = 3;
    int32 first = 4;
    string after = 5;
}

message CategoryResponse {
    string id = 1;
    string name = 2;
    string slug = 3;
    string user_id = 4;
}

message ListCategory {
    repeated CategoryResponse data = 1;
}

message ArticleCategoryRequest {
    string article_id = 1;
    string category_id = 2;
}

message SearchRequest {
    string query = 1;
    int32 first = 9;
    string after = 10;
}

service ArticleService {
    rpc SearchArticle(SearchRequest) returns (ListArticle) {}

    rpc CreateArticle(ArticleRequest) returns (ArticleResponse) {}
    rpc UpdateArticle(ArticleRequest) returns (ArticleResponse) {}
    rpc GetArticleByID(ArticleRequest) returns (ArticleResponse) {}
    rpc GetArticleBySlug(ArticleRequest) returns (ArticleResponse) {}

    rpc GetPublishedArticles(ArticleRequest) returns (ListArticle) {}
    rpc GetPublishedArticlesByUserID(ArticleRequest) returns (ListArticle) {}
    rpc GetPublishedArticlesByCategorySlug(ArticleRequest) returns (ListArticle) {}
    rpc GetPublishedArticlesByUserIDAndCategorySlug(ArticleRequest) returns (ListArticle) {}
    rpc GetPublishedRelatedArticles(RelatedArticleRequest) returns (ListArticle) {}

    rpc GetDraftedArticlesByUserID(ArticleRequest) returns (ListArticle) {}

    rpc PublishArticle(ArticleRequest) returns (ArticleResponse) {}
    rpc AddCategory(ArticleCategoryRequest) returns (ArticleResponse) {}
    rpc RemoveCategory(ArticleCategoryRequest) returns (ArticleResponse) {}

    rpc CreateCategory(CategoryRequest) returns (CategoryResponse) {}
    rpc GetCategoryByID(CategoryRequest) returns (CategoryResponse) {}
    rpc GetCategoriesByUserID(CategoryRequest) returns (ListCategory) {}
    rpc GetDefaultCategories(CategoryRequest) returns (ListCategory) {}
}