package handler

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
	"gitlab.com/veroskevin/miku-backend/graphql/connector"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"

	"github.com/labstack/echo"
)

// LoginHandler is to handle login
func LoginHandler(c echo.Context) error {
	var err error
	var result *pb.UserResponse
	login := struct {
		UsernameOrEmail string `json:"username_or_email"`
		Password        string `json:"password"`
	}{}
	json.NewDecoder(c.Request().Body).Decode(&login)

	user := connector.NewUserServiceGrpcClient(config.GrpcUserServicePort)
	if strings.Contains(login.UsernameOrEmail, "@") {
		req := &pb.UserRequest{Email: login.UsernameOrEmail}
		result, err = user.GetUserByEmail(context.Background(), req)
	} else {
		req := &pb.UserRequest{Username: login.UsernameOrEmail}
		result, err = user.GetUserByUsername(context.Background(), req)
	}
	if err != nil || result.Password != login.Password {
		return c.JSON(http.StatusUnauthorized, struct{ Error string }{"Unauthorized"})
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = result.Id
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(config.JwtSecretKey))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"token": t,
		"user":  result,
	})
}
