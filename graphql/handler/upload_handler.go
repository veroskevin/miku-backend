package handler

import (
	"github.com/labstack/echo"
	"gitlab.com/veroskevin/miku-backend/graphql/service/cloudinary"
	"io"
	"net/http"
	"os"
)

// UploadFileHandler is handler for uploading file
func UploadFileHandler(c echo.Context) error {
	file, err := c.FormFile("image")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}

	defer src.Close()

	// Destination
	dst, err := os.Create(file.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	service := cloudinary.GetService()
	url, err := service.UploadFile("mikum", dst.Name(), nil)
	if err != nil {
		return err
	}
	os.Remove(dst.Name())
	return c.JSON(http.StatusOK, struct {
		URL string `json:"url"`
	}{URL: url})
}
