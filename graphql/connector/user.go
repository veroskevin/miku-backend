package connector

import (
	"log"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc"
)

// NewUserServiceGrpcClient return new grpc user service client
func NewUserServiceGrpcClient(port string) pb.UserServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewUserServiceClient(conn)
}
