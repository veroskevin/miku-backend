package connector

import (
	"log"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc"
)

// NewArticleServiceGrpcClient return new grpc article service client
func NewArticleServiceGrpcClient(port string) pb.ArticleServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewArticleServiceClient(conn)
}
