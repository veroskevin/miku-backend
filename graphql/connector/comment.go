package connector

import (
	"log"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc"
)

// NewCommentServiceGrpcClient return new grpc comment service client
func NewCommentServiceGrpcClient(port string) pb.CommentServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewCommentServiceClient(conn)
}
