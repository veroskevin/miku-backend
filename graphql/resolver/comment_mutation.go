package resolver

import (
	"context"
	"errors"

	"gitlab.com/veroskevin/miku-backend/graphql/config"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc/status"
)

// CommentInput is model input
type CommentInput struct {
	Body      string
	ArticleID string
}

// CreateComment is to resolve create model mutation
func (r *Resolver) CreateComment(ctx context.Context, args struct{ Comment CommentInput }) (*CommentResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	currentUser, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, err
	}

	req := &pb.CommentRequest{
		Body:      args.Comment.Body,
		ArticleId: args.Comment.ArticleID,
		UserId:    currentUser.Id,
	}
	comment, err := r.CommentService.CreateComment(context.Background(), req)
	if err != nil {
		status := status.Convert(err)
		return nil, errors.New(status.Message())
	}

	return &CommentResolver{
		model: comment,
	}, nil
}
