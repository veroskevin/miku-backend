package resolver

import (
	"context"
	"errors"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc/status"
)

// User is to resolve model query
func (r *Resolver) User(ctx context.Context, args struct{ Username string }) (*UserResolver, error) {
	user, err := r.UserService.GetUserByUsername(context.Background(), &pb.UserRequest{Username: args.Username})
	if err != nil {
		status := status.Convert(err)
		return nil, errors.New(status.Message())
	}

	return &UserResolver{
		model: user,
	}, nil
}

