package resolver

import (
	"context"
	"errors"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc/status"
)

// CategoryInput is article input
type CategoryInput struct {
	Name string
	Slug string
}

// CreateCategory is to resolve create category mutation
func (r *Resolver) CreateCategory(ctx context.Context, args struct{ Category CategoryInput }) (*CategoryResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	user, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, ErrUnauthorized
	}

	req := &pb.CategoryRequest{
		Name:   args.Category.Name,
		Slug:   args.Category.Slug,
		UserId: user.Id,
	}
	category, err := r.ArticleService.CreateCategory(context.Background(), req)
	if err != nil {
		status := status.Convert(err)
		return nil, errors.New(status.Message())
	}

	return &CategoryResolver{
		category: category,
	}, nil
}
