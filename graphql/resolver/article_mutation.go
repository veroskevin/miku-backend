package resolver

import (
	"context"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// ArticleInput is article input
type ArticleInput struct {
	ImageURL string
	Title    string
	Body     string
	Slug     *string
}

// ArticleUpdate is article update input
type ArticleUpdate struct {
	ID       string
	ImageURL string
	Title    string
	Body     string
	Slug     string
}

// CreateArticle is to resolve create article mutation
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	currentUser, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, err
	}

	if args.Article.Slug == nil {
		args.Article.Slug = new(string)
	}
	req := &pb.ArticleRequest{
		ImageUrl: args.Article.ImageURL,
		Title:    args.Article.Title,
		Body:     args.Article.Body,
		Slug:     *args.Article.Slug,
		UserId:   currentUser.Id,
	}
	article, err := r.ArticleService.CreateArticle(context.Background(), req)
	if err != nil {
		return nil, parseGrpcError(err)
	}
	return &ArticleResolver{model: article}, nil
}

// AddCategory add category to article
func (r *Resolver) AddCategory(ctx context.Context, args struct {
	ArticleID  string
	CategoryID string
}) (*ArticleResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	_, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, ErrUnauthorized
	}

	req := &pb.ArticleCategoryRequest{
		ArticleId:  args.ArticleID,
		CategoryId: args.CategoryID,
	}
	article, err := r.ArticleService.AddCategory(context.Background(), req)
	return &ArticleResolver{
		model: article,
	}, nil
}

// RemoveCategory in article
func (r *Resolver) RemoveCategory(ctx context.Context, args struct {
	ArticleID  string
	CategoryID string
}) (*ArticleResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	_, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, ErrUnauthorized
	}

	req := &pb.ArticleCategoryRequest{
		ArticleId:  args.ArticleID,
		CategoryId: args.CategoryID,
	}
	article, err := r.ArticleService.RemoveCategory(context.Background(), req)
	if err != nil {
		return nil, parseGrpcError(err)
	}

	return &ArticleResolver{
		model: article,
	}, nil
}

// PublishArticle is to publish article
func (r *Resolver) PublishArticle(ctx context.Context, args struct{ ID string }) (*ArticleResolver, error) {
	currentUserID := ctx.Value(config.CurrentUserID).(string)
	user, err := getCurrentUser(r.UserService, currentUserID)
	if err != nil {
		return nil, ErrUnauthorized
	}
	article, err := r.ArticleService.GetArticleByID(context.Background(), &pb.ArticleRequest{Id: args.ID})
	if err != nil {
		return nil, parseGrpcError(err)
	}
	if article.UserId != user.Id {
		return nil, ErrUnauthorized
	}

	article, err = r.ArticleService.PublishArticle(context.Background(), &pb.ArticleRequest{Id: args.ID})
	if err != nil {
		return nil, parseGrpcError(err)
	}
	return &ArticleResolver{model: article}, nil
}

// UpdateArticle is to resolve upadteArticle mutation
func (r *Resolver) UpdateArticle(ctx context.Context, args struct{ Article ArticleUpdate }) (*ArticleResolver, error) {
	req := &pb.ArticleRequest{
		Id:       args.Article.ID,
		ImageUrl: args.Article.ImageURL,
		Title:    args.Article.Title,
		Slug:     args.Article.Slug,
		Body:     args.Article.Body,
	}
	article, err := r.ArticleService.UpdateArticle(context.Background(), req)
	if err != nil {
		return nil, parseGrpcError(err)
	}
	return &ArticleResolver{model: article}, nil
}
