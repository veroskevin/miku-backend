package resolver

import (
	"context"
	"errors"
	"google.golang.org/grpc/status"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// Error
var (
	ErrUnauthorized = errors.New("Unathorized")
)

// ClienServices resolver
var (
	Client *Resolver
)

// Resolver is root resolver
type Resolver struct {
	// GRPC model service
	UserService    pb.UserServiceClient
	ArticleService pb.ArticleServiceClient
	CommentService pb.CommentServiceClient
}

func InitResolver() {
	Client = new(Resolver)
}

func (r *Resolver) SetUserServiceClient(c pb.UserServiceClient) {
	r.UserService = c
}

func (r *Resolver) SetArticleServiceClient(c pb.ArticleServiceClient) {
	r.ArticleService = c
}

func (r *Resolver) SetCommentServiceClient(c pb.CommentServiceClient) {
	r.CommentService = c
}

func getCurrentUser(s pb.UserServiceClient, id string) (*pb.UserResponse, error) {
	if id == "" {
		return nil, ErrUnauthorized
	}
	user, err := s.GetUserByID(context.Background(), &pb.UserRequest{Id: id})
	if err != nil {
		return nil, ErrUnauthorized
	}
	return user, nil
}

func stringValue(v *string) string {
	if v == nil {
		return ""
	}
	return *v
}

func parseGrpcError(err error) error {
	s := status.Convert(err)
	return errors.New(s.Message())
}
