package resolver

import (
	"context"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// Search is to resolve search article query
func (r *Resolver) Search(ctx context.Context, args struct {
	Query string
	After *string
	First int32
}) ([]ArticleResolver, error) {
	after := stringValue(args.After)
	if after == "" {
		after = "0"
	}
	req := &pb.SearchRequest{
		After: after,
		First: args.First,
		Query: args.Query,
	}
	list, err := r.ArticleService.SearchArticle(context.Background(), req)
	if err != nil {
		return nil, err
	}
	var result []ArticleResolver
	for _, v := range list.Data {
		res := ArticleResolver{model: v}
		result = append(result, res)
	}
	return result, nil
}
