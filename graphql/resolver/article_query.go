package resolver

import (
	"context"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// Article status
const (
	StatusPublished = "PUBLISHED"
	StatusDraft     = "DRAFT"
)

// Article is to resolve article query
func (r *Resolver) Article(ctx context.Context, args struct{ Slug string }) (*ArticleResolver, error) {
	article, err := r.ArticleService.GetArticleBySlug(context.Background(), &pb.ArticleRequest{Slug: args.Slug})
	if err != nil {
		return nil, parseGrpcError(err)
	}

	return &ArticleResolver{
		model: article,
	}, nil
}

// Articles is to resolve articles query
func (r *Resolver) Articles(ctx context.Context, args struct {
	Status       string
	UserID       *string
	CategorySlug *string
	First        int32
	After        *string
}) ([]ArticleResolver, error) {
	var list *pb.ListArticle
	var err error
	after := stringValue(args.After)
	if after == "" {
		after = "0"
	}

	if args.Status == StatusPublished {
		if args.UserID != nil && args.CategorySlug != nil {
			req := &pb.ArticleRequest{
				UserId:       *args.UserID,
				CategorySlug: *args.CategorySlug,
				First:        args.First,
				After:        after,
			}
			list, err = r.ArticleService.GetPublishedArticlesByUserIDAndCategorySlug(context.Background(), req)
		} else if args.UserID != nil {
			req := &pb.ArticleRequest{
				UserId: *args.UserID,
				First:  args.First,
				After:  after,
			}
			list, err = r.ArticleService.GetPublishedArticlesByUserID(context.Background(), req)
		} else if args.CategorySlug != nil {
			req := &pb.ArticleRequest{
				CategorySlug: *args.CategorySlug,
				First:        args.First,
				After:        after,
			}
			list, err = r.ArticleService.GetPublishedArticlesByCategorySlug(context.Background(), req)
		} else {
			req := &pb.ArticleRequest{
				First: args.First,
				After: after,
			}
			list, err = r.ArticleService.GetPublishedArticles(context.Background(), req)
		}
	} else {
		currentUserID := ctx.Value(config.CurrentUserID).(string)
		user, err := getCurrentUser(r.UserService, currentUserID)
		if err != nil {
			return nil, err
		}

		req := &pb.ArticleRequest{
			UserId: user.Id,
			First:  args.First,
			After:  after,
		}
		list, err = r.ArticleService.GetDraftedArticlesByUserID(context.Background(), req)
	}
	if err != nil {
		return nil, parseGrpcError(err)
	}

	var result []ArticleResolver
	for _, v := range list.Data {
		res := ArticleResolver{model: v}
		result = append(result, res)
	}
	return result, nil
}

// RelatedArticles is to resolve relatedArticles query
func (r *Resolver) RelatedArticles(ctx context.Context, args struct {
	Title      string
	Categories *[]*string
}) ([]ArticleResolver, error) {
	var categories []string
	if args.Categories != nil {
		for _, v := range *args.Categories {
			categories = append(categories, *v)
		}
	}
	req := &pb.RelatedArticleRequest{
		UserId:       args.Title,
		CategorySlug: categories,
	}
	list, err := r.ArticleService.GetPublishedRelatedArticles(context.Background(), req)
	if err != nil {
		return nil, err
	}

	var result []ArticleResolver
	for _, v := range list.Data {
		res := ArticleResolver{model: v}
		result = append(result, res)
	}
	return result, nil
}
