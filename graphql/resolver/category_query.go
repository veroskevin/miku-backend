package resolver

import (
	"context"
	"errors"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
	"google.golang.org/grpc/status"
)

// Categories is to resolve categories query
func (r *Resolver) Categories(ctx context.Context, args struct {
	UserID *string
	First  int32
	After  *string
}) ([]CategoryResolver, error) {
	var list *pb.ListCategory
	var err error
	after := stringValue(args.After)
	if after == "" {
		after = "0"
	}
	if args.UserID == nil {
		req := &pb.CategoryRequest{
			First: args.First,
			After: after,
		}
		list, err = r.ArticleService.GetDefaultCategories(context.Background(), req)
	} else {
		req := &pb.CategoryRequest{
			UserId: *args.UserID,
			First:  args.First,
			After:  after,
		}
		list, err = r.ArticleService.GetCategoriesByUserID(context.Background(), req)
	}
	if err != nil {
		status := status.Convert(err)
		return nil, errors.New(status.Message())
	}

	var result []CategoryResolver
	for _, v := range list.Data {
		res := CategoryResolver{category: v}
		result = append(result, res)
	}
	return result, nil
}
