package resolver

import (
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// UserResolver is resolver for model
type UserResolver struct {
	model *pb.UserResponse
}

// ID is to resolve id
func (r *UserResolver) ID() string {
	return r.model.GetId()
}

// Name is to resolve name
func (r *UserResolver) Name() string {
	return r.model.GetName()
}

// Username is to resolve username
func (r *UserResolver) Username() string {
	return r.model.GetUsername()
}

// Email is to resolve email
func (r *UserResolver) Email() string {
	return r.model.GetEmail()
}
