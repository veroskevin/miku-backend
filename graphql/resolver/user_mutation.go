package resolver

import (
	"context"
	"errors"
	"google.golang.org/grpc/status"

	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// UserInput hold model input data
type UserInput struct {
	Name     string
	Username string
	Email    string
	Password string
}

// CreateUser resolve CreateUser mutation
func (r *Resolver) CreateUser(ctx context.Context, args struct{ User UserInput }) (*UserResolver, error) {
	req := &pb.UserRequest{
		Name:     args.User.Name,
		Username: args.User.Username,
		Email:    args.User.Email,
		Password: args.User.Password,
	}
	result, err := r.UserService.CreateUser(context.Background(), req)
	if err != nil {
		status := status.Convert(err)
		return nil, errors.New(status.Message())
	}

	return &UserResolver{
		model: result,
	}, nil
}
