package resolver

import (
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// CategoryResolver is resolver for category
type CategoryResolver struct {
	category *pb.CategoryResponse
}

// ID is to resolve id
func (r CategoryResolver) ID() string {
	return r.category.Id
}

// Name is to resolve name
func (r CategoryResolver) Name() string {
	return r.category.Name
}

// Slug is to resolve id
func (r CategoryResolver) Slug() string {
	return r.category.Slug
}

// UserID is to resolve userId
func (r CategoryResolver) UserID() string {
	return r.category.UserId
}
