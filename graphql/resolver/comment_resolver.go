package resolver

import (
	"context"
	"fmt"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// CommentResolver is resolver for model
type CommentResolver struct {
	model *pb.CommentResponse
}

// ID is to resolve model Id
func (r CommentResolver) ID() string {
	return r.model.Id
}

// Body is to resolve model Body
func (r CommentResolver) Body() string {
	return r.model.Body
}

// ArticleID is to resolve model ArticleId
func (r CommentResolver) ArticleID() string {
	return r.model.ArticleId
}

// User is to resolve model in model
func (r CommentResolver) User() (*UserResolver, error) {
	user, err := Client.UserService.GetUserByID(context.Background(), &pb.UserRequest{Id: r.model.UserId})
	if err != nil {
		return nil, err
	}
	return &UserResolver{model: user}, nil
}

// CreatedAt is to resolve article createdAt
func (r CommentResolver) CreatedAt() string {
	return fmt.Sprint(r.model.CreatedAt.Seconds * 1000)
}

// UpdatedAt is to resolve article UpdatedAt
func (r CommentResolver) UpdatedAt() string {
	return fmt.Sprint(r.model.UpdatedAt.Seconds * 1000)
}
