package resolver

import (
	"context"
	"fmt"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// ArticleResolver is resolver for article
type ArticleResolver struct {
	model *pb.ArticleResponse
}

// ID is to resolve article id
func (r ArticleResolver) ID() string {
	return r.model.Id
}

// ImageURL is to resolve article image url
func (r ArticleResolver) ImageURL() string {
	return r.model.ImageUrl
}

// Title is to resolve article title
func (r ArticleResolver) Title() string {
	return r.model.Title
}

// Body is to resolve article body
func (r ArticleResolver) Body() string {
	return r.model.Body
}

// Slug is to resolve article slug
func (r ArticleResolver) Slug() string {
	return r.model.Slug
}

// Status is to resolve article status
func (r ArticleResolver) Status() string {
	return r.model.Status
}

// CreatedAt is to resolve article createdAt
func (r ArticleResolver) CreatedAt() string {
	return fmt.Sprint(r.model.CreatedAt.Seconds * 1000)
}

// UpdatedAt is to resolve article UpdatedAt
func (r ArticleResolver) UpdatedAt() string {
	return fmt.Sprint(r.model.UpdatedAt.Seconds * 1000)
}

// Categories is to resolve article categories
func (r ArticleResolver) Categories() []CategoryResolver {
	result := make([]CategoryResolver, len(r.model.Categories))
	for i, v := range r.model.Categories {
		result[i] = CategoryResolver{
			category: v,
		}
	}
	return result
}

// User is to resolve model in article
func (r ArticleResolver) User() (*UserResolver, error) {
	user, err := Client.UserService.GetUserByID(context.Background(), &pb.UserRequest{Id: r.model.UserId})
	if err != nil {
		return nil, err
	}
	return &UserResolver{model: user}, nil
}

// Comments is to resolve article comments
func (r ArticleResolver) Comments() ([]CommentResolver, error) {
	after := "0"
	req := &pb.CommentRequest{
		ArticleId: r.model.Id,
		First: 10000,
		After: after,
	}
	result, err := Client.CommentService.GetCommentsByArticleID(context.Background(), req)
	if err != nil {
		return nil, err
	}
	var comments []CommentResolver
	for _, v := range result.Data {
		res := CommentResolver{model: v}
		comments = append(comments, res)
	}
	return comments, nil
}

// TotalComments is to resolve total comments in one article
func (r ArticleResolver) TotalComments() int32 {
	result, err := Client.CommentService.CountByArticleID(context.Background(), &pb.CommentRequest{ArticleId: r.model.Id})
	if err != nil {
		return 0
	}
	return result.Data
}
