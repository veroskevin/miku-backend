package resolver

import (
	"context"
	pb "gitlab.com/veroskevin/miku-backend/graphql/proto"
)

// Comments will return comments with articleID like what provided
func (r *Resolver) Comments(args struct {
	ArticleID string
	First     int32
	After     *string
}) ([]CommentResolver, error) {
	after := stringValue(args.After)
	if after == "" {
		after = "0"
	}
	req := &pb.CommentRequest{
		ArticleId: args.ArticleID,
		First:     args.First,
		After:    after,
	}

	comments, err := r.CommentService.GetCommentsByArticleID(context.Background(), req)
	if err != nil {
		return nil, err
	}

	var result []CommentResolver
	for _, v := range comments.Data {
		res := CommentResolver{model: v}
		result = append(result, res)
	}
	return result, nil
}
