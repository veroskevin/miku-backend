package config

// Application configuration
var (
	GrpcUserServicePort    = "user:9002"
	GrpcArticleServicePort = "article:9000"
	GrpcCommentServicePort = "comment:9001"
	GrapQLServerPort       = ":8080"
	JwtSecretKey           = "secret"
)

// const
const (
	CurrentUserID = "UserID"
)
