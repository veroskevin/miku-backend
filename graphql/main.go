package main

import (
	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
	"gitlab.com/veroskevin/miku-backend/graphql/connector"
	"gitlab.com/veroskevin/miku-backend/graphql/handler"
	m "gitlab.com/veroskevin/miku-backend/graphql/middleware"
	"gitlab.com/veroskevin/miku-backend/graphql/resolver"
	"html/template"
	"io"
	"io/ioutil"
	"log"
)

// Get schema helper
func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// Template renderer
type Template struct {
	templates *template.Template
}

// Render template
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {
	resolver.InitResolver()
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}

	userClient := connector.NewUserServiceGrpcClient(config.GrpcUserServicePort)
	articleClient := connector.NewArticleServiceGrpcClient(config.GrpcArticleServicePort)
	commentClient := connector.NewCommentServiceGrpcClient(config.GrpcCommentServicePort)

	resolver.Client.SetUserServiceClient(userClient)
	resolver.Client.SetArticleServiceClient(articleClient)
	resolver.Client.SetCommentServiceClient(commentClient)
	schema := graphql.MustParseSchema(s, resolver.Client)

	e := echo.New()
	t := &Template{
		templates: template.Must(template.ParseGlob("./*.html")),
	}
	e.Renderer = t
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	e.POST("/login", handler.LoginHandler)
	e.POST("/upload", handler.UploadFileHandler)
	e.POST("/graphql", m.AuthJWT(&relay.Handler{Schema: schema}))
	e.GET("/graphql", func(c echo.Context) error {
		return c.Render(200, "index.html", nil)
	})
	log.Fatal(e.Start(config.GrapQLServerPort))
}
