package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"gitlab.com/veroskevin/miku-backend/graphql/config"
)

// AuthJWT is middleware for auth
func AuthJWT(h http.Handler) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Original Request
		originalRequest := c.Request()
		originalContext := originalRequest.Context()

		var userID string

		authorizationHeader := c.Request().Header.Get("Authorization")
		if strings.Contains(authorizationHeader, "Bearer") {
			tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

			token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Signing method invalid")
				} else if method != jwt.SigningMethodHS256 {
					return nil, fmt.Errorf("Signing method invalid")
				}

				return config.JwtSecretKey, nil
			})

			claims, _ := token.Claims.(jwt.MapClaims)

			userID = claims["id"].(string)
		}

		req := originalRequest.WithContext(
			context.WithValue(originalContext, config.CurrentUserID, userID),
		)

		h.ServeHTTP(c.Response(), req)
		return nil
	}
}
