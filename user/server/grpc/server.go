package grpc

import (
	"context"
	"github.com/satori/go.uuid"
	pb "gitlab.com/veroskevin/miku-backend/user/proto"
	"gitlab.com/veroskevin/miku-backend/user/service"
)

// UserServer is the implementor of
// UserServiceServer in proto
type UserServer struct {
	Service *service.UserService
}

// NewUserServer used to create a new user server
func NewUserServer(service *service.UserService) *UserServer {
	return &UserServer{
		Service: service,
	}
}

// CreateUser used to create a new user
func (s *UserServer) CreateUser(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := s.Service.CreateUser(param.Name, param.Username, param.Email, param.Password)
	if err != nil {
		return nil, err
	}

	return user.Proto(), nil
}

// GetUserByEmail used to get the user by its email
func (s *UserServer) GetUserByEmail(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := s.Service.GetUserByEmail(param.Email)
	if err != nil {
		return nil, err
	}

	return user.Proto(), nil
}

// GetUserByUsername used to get the user by its username
func (s *UserServer) GetUserByUsername(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	user, err := s.Service.GetUserByUsername(param.Username)
	if err != nil {
		return nil, err
	}

	return user.Proto(), nil
}

// GetUserByID used to get the user by its id
func (s *UserServer) GetUserByID(ctx context.Context, param *pb.UserRequest) (*pb.UserResponse, error) {
	id, _ := uuid.FromString(param.Id)
	user, err := s.Service.GetUserByID(id)
	if err != nil {
		return nil, err
	}

	return user.Proto(), nil
}
