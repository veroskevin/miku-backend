package config

import (
	"encoding/json"
	"github.com/go-redis/redis"
	"log"
	"time"
)

type Redis struct {
	url string
	db  int
}

func NewRedis(url string, db int) *Redis {
	return &Redis{url, db}
}

func newPool(url string, db int) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     url,
		Password: "", // no password set
		DB:       0,
	})
	_, err := client.Ping().Result()
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return client, nil
}

// Set redis data followed with log
func (r *Redis) Set(k string, v interface{}, t time.Duration) {
	client, err := newPool(r.url, r.db)
	if err != nil {
		log.Println("[REDIS] ERROR", err)
		return
	}
	defer client.Close()
	data, err := json.Marshal(v)
	if err == nil {
		if err = client.Set(k, data, t).Err(); err != nil {
			log.Println("[REDIS] ERROR", err)
			return
		}
		log.Println("[REDIS] SET", k)
	}
}

// Get redis data followed with log
func (r *Redis) Get(k string) string {
	client, err := newPool(r.url, r.db)
	if err != nil {
		log.Println("[REDIS] ERROR", err)
		return ""
	}
	defer client.Close()
	result, err := client.Get(k).Result()
	ttl := client.TTL(k).Val()
	if err == nil {
		log.Println("[REDIS] GET", k, "ttl:", ttl)
	}
	return result
}
