package config

import (
	"database/sql"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
)

// Application configuration
var (
	GrpcUserServicePort = ":9002"
	DBSchemaPath        = "./database/schema.sql"
	RedisURL            = "redis:6379"
	RedisDB             = 2
)

// InitDB schema
func InitDB() (*sql.DB, error) {
	db, err := sql.Open("postgres", "postgresql://root@cockroach:26257?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	_, err = db.Exec(`CREATE DATABASE IF NOT EXISTS user_service`)
	db.Close()

	db, err = sql.Open("postgres", "postgresql://root@cockroach:26257/user_service?sslmode=disable")
	err = db.Ping()
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	src, err := ioutil.ReadFile(DBSchemaPath)
	schema := string(src)
	if err != nil {
		return nil, err
	}
	_, err = db.Exec(schema)
	if err != nil {
		return nil, err
	}
	log.Println("InitDB: connected to database ...")
	return db, nil
}
