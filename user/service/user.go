package service

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/config"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/domain/helper"
	"gitlab.com/veroskevin/miku-backend/user/repository"
	"gitlab.com/veroskevin/miku-backend/user/repository/cockroach"
	"log"
)

// UserService is service for user domain
type UserService struct {
	Query      repository.UserQuery
	Repository repository.UserRepository
}

// NewUserService return new UserService
func NewUserService() *UserService {
	db, err := config.InitDB()
	if err != nil {
		log.Fatal("NewUserService: ", err)
	}
	query := cockroach.NewUserQueryCockroach(db, config.NewRedis(config.RedisURL, config.RedisDB))
	repo := cockroach.NewUserRepositoryCockroach(db)

	return &UserService{
		Query:      query,
		Repository: repo,
	}
}

// CreateUser used to create a new user
func (s *UserService) CreateUser(name, username, email, password string) (*domain.User, error) {
	helper := helper.NewUserHelperImpl(s.Query)
	user, err := domain.NewUser(helper, name, username, email, password)
	if err != nil {
		return nil, err
	}

	err = <-s.Repository.Create(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetUserByEmail used to get the user by its email
func (s *UserService) GetUserByEmail(email string) (user domain.User, err error) {
	result := <-s.Query.GetByEmail(email)

	if result.Error != nil {
		return user, result.Error
	}

	return result.Result.(domain.User), nil
}

// GetUserByUsername used to get the user by its username
func (s *UserService) GetUserByUsername(username string) (user domain.User, err error) {
	result := <-s.Query.GetByUsername(username)
	if result.Error != nil {
		return user, result.Error
	}

	return result.Result.(domain.User), nil
}

// GetUserByID return user by id
func (s *UserService) GetUserByID(id uuid.UUID) (domain.User, error) {
	result := <-s.Query.GetByID(id)
	if result.Error != nil {
		return domain.User{}, result.Error
	}

	return result.Result.(domain.User), nil
}
