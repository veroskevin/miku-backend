package domain

import (
	"github.com/satori/go.uuid"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/veroskevin/miku-backend/user/domain/mock"
	pb "gitlab.com/veroskevin/miku-backend/user/proto"
)

func TestProto(t *testing.T) {
	// Given
	id := uuid.NewV4()
	name := "name"
	username := "username"
	email := "email@email.com"
	password := "password"
	user := &User{
		ID:       id,
		Name:     name,
		Username: username,
		Email:    email,
		Password: password,
	}
	want := &pb.UserResponse{
		Id:       user.ID.String(),
		Name:     user.Name,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	}

	// When
	got := user.Proto()

	// Then
	assert.Equal(t, want, got)
}

func TestNewUser(t *testing.T) {
	name := "name"
	username := "username"
	email := "email"
	password := "password"

	t.Run("Can create user", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockUserHelper(controller)
		m.EXPECT().EmailExists(email).Return(nil).AnyTimes()
		m.EXPECT().UsernameExists(username).Return(nil).AnyTimes()

		got, err := NewUser(m, name, username, email, password)

		assert.Nil(t, err)
		assert.NotNil(t, got)
	})

	t.Run("Can't create user if username not exists", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockUserHelper(controller)
		m.EXPECT().EmailExists(email).Return(nil).AnyTimes()
		m.EXPECT().UsernameExists(username).Return(ErrUsernameExists).AnyTimes()

		got, err := NewUser(m, name, username, email, password)

		assert.Equal(t, ErrUsernameExists, err)
		assert.Nil(t, got)
	})

	t.Run("Can't create user if email not exists", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockUserHelper(controller)
		m.EXPECT().EmailExists(email).Return(ErrEmailExists).AnyTimes()
		m.EXPECT().UsernameExists(username).Return(nil).AnyTimes()

		got, err := NewUser(m, name, username, email, password)

		assert.Equal(t, ErrEmailExists, err)
		assert.Nil(t, got)
	})

	t.Run("Can't create user if password less than minimum password", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockUserHelper(controller)
		m.EXPECT().EmailExists(email).Return(nil).AnyTimes()
		m.EXPECT().UsernameExists(username).Return(nil).AnyTimes()
		password = "123"

		got, err := NewUser(m, name, username, email, password)

		assert.Nil(t, got)
		assert.Equal(t, ErrMinimumPasswordLength, err)
	})
}
