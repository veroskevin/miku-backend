package helper

import (
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/repository"
)

// UserHelperImpl is in memory user helper implementation
type UserHelperImpl struct {
	Query repository.UserQuery
}

// NewUserHelperImpl return new user helper inmemory
func NewUserHelperImpl(query repository.UserQuery) domain.UserHelper {
	return &UserHelperImpl{
		Query: query,
	}
}

// EmailExists is to check if email exists or not
func (h *UserHelperImpl) EmailExists(email string) error {
	result := <-h.Query.GetByEmail(email)
	if result.Error == nil {
		return domain.ErrEmailExists
	}
	return nil
}

// UsernameExists is to check if username exists or not
func (h *UserHelperImpl) UsernameExists(username string) error {
	result := <-h.Query.GetByUsername(username)
	if result.Error == nil {
		return domain.ErrUsernameExists
	}
	return nil
}
