package domain

import "errors"

var (
	// ErrUserNotFound = system cannot found the user requested
	ErrUserNotFound = errors.New("User not found")
	// ErrEmailExists = system found out that the email has been registered
	ErrEmailExists = errors.New("Email has been registered")
	// ErrUsernameExists = system found out that the username has been registered
	ErrUsernameExists = errors.New("Username has been registered")
	// ErrMinimumPasswordLength = system found out that the password is not long enough
	ErrMinimumPasswordLength = errors.New("Password is not long enough")
)
