package domain

import (
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/veroskevin/miku-backend/user/proto"
)

const (
	// MinimumPasswordLength = the minimum
	// password length the user must provide
	MinimumPasswordLength = 5
)

// User represent user specification
type User struct {
	ID       uuid.UUID
	Name     string
	Username string
	Email    string
	Password string
}

// UserHelper is helper fo user domain
type UserHelper interface {
	EmailExists(string) error
	UsernameExists(string) error
}

// Proto used to convert user struct to proto
func (u *User) Proto() *pb.UserResponse {
	return &pb.UserResponse{
		Id:       u.ID.String(),
		Name:     u.Name,
		Username: u.Username,
		Email:    u.Email,
		Password: u.Password,
	}
}

// NewUser used to create a new user
func NewUser(h UserHelper, name, username, email, password string) (*User, error) {
	if err := h.EmailExists(email); err != nil {
		return nil, err
	}
	if err := h.UsernameExists(username); err != nil {
		return nil, err
	}

	if len(password) < MinimumPasswordLength {
		return nil, ErrMinimumPasswordLength
	}

	return &User{
		ID:       uuid.NewV4(),
		Name:     name,
		Username: username,
		Email:    email,
		Password: password,
	}, nil
}
