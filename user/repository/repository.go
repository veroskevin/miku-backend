package repository

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/domain"
)

// UserRepository interface
type UserRepository interface {
	Create(*domain.User) chan error
	Update(uuid.UUID, domain.User) chan error
}
