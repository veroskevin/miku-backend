package cockroach

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"testing"
)

func TestCreate(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	repo := NewUserRepositoryCockroach(db)

	id := uuid.NewV4()
	name := "name"
	username := "username"
	email := "email@email.com"
	password := "123456"
	user := &domain.User{
		ID:       id,
		Name:     name,
		Username: username,
		Email:    email,
		Password: password,
	}

	t.Run("Insert success", func(t *testing.T) {
		mock.ExpectExec(`INSERT INTO users`).
			WithArgs(id, name, username, email, password).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := <-repo.Create(user)
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("Insert fail", func(t *testing.T) {
		mock.ExpectExec(`INSERT INTO users`).
			WithArgs(id, name, username, email, password).
			WillReturnError(domain.ErrUsernameExists)

		err := <-repo.Create(user)
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

}

func TestUpdate(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	repo := NewUserRepositoryCockroach(db)

	id := uuid.NewV4()
	name := "name"
	email := "email@email.com"
	password := "123456"
	user := &domain.User{
		ID:       id,
		Name:     name,
		Email:    email,
		Password: password,
	}

	t.Run("Update success", func(t *testing.T) {
		mock.ExpectExec(`UPDATE users`).
			WithArgs(name, email, password, id).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := <-repo.Update(id, *user)
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("Update fail", func(t *testing.T) {
		mock.ExpectExec(`UPDATE users`).
			WithArgs(name, email, password, id).
			WillReturnError(sql.ErrConnDone)

		err := <-repo.Update(id, *user)
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}
