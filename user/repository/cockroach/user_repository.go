package cockroach

import (
	"database/sql"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/repository"
)

// UserRepositoryCockroach is user repository cockroachdb implementation
type UserRepositoryCockroach struct {
	db *sql.DB
}

// NewUserRepositoryCockroach return new user query cockrach implementation
func NewUserRepositoryCockroach(db *sql.DB) repository.UserRepository {
	return &UserRepositoryCockroach{
		db: db,
	}
}

// Create is to insert data to database
func (r *UserRepositoryCockroach) Create(user *domain.User) chan error {
	result := make(chan error)
	go func() {
		query := `
		INSERT INTO users(id, name, username, email, password)
		VALUES ($1, $2, $3, $4, $5)	
	`
		_, err := r.db.Exec(
			query, user.ID, user.Name,
			user.Username, user.Email, user.Password,
		)
		if err != nil {
			result <- err
		}
		close(result)
	}()

	return result
}

// Update is to upadate article data in database
func (r *UserRepositoryCockroach) Update(id uuid.UUID, user domain.User) chan error {
	result := make(chan error)
	go func() {
		query := `
			UPDATE users
			SET name=$1, email=$2, password=$3
			WHERE id=$4
		`
		_, err := r.db.Exec(
			query, user.Name, user.Email, user.Password, id,
		)
		if err != nil {
			result <- err
		}
		close(result)
	}()

	return result
}
