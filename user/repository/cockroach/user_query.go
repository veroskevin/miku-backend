package cockroach

import (
	"database/sql"
	"encoding/json"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/config"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/repository"
	"time"
)

// UserQueryCockroach is user query cockroach implementation
type UserQueryCockroach struct {
	db    *sql.DB
	cache *config.Redis
}

// NewUserQueryCockroach return new user query cockrach implementation
func NewUserQueryCockroach(db *sql.DB, cache *config.Redis) repository.UserQuery {
	return &UserQueryCockroach{
		db:    db,
		cache: cache,
	}
}

// GetByUsername return user by username
func (q *UserQueryCockroach) GetByUsername(username string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	key := "user-" + username
	go func() {
		var user domain.User
		if v := q.cache.Get(key); v != "" {
			err := json.Unmarshal([]byte(v), &user)
			if err != nil {
				result <- repository.QueryResult{Error: err}
				close(result)
			}
			result <- repository.QueryResult{Result: user}
			close(result)
			return
		}

		query := `
			SELECT id, name, username, email, password
			FROM users
			WHERE username=$1
		`
		err := q.db.QueryRow(
			query, username,
		).Scan(&user.ID, &user.Name, &user.Username, &user.Email, &user.Password)

		if err != nil {
			result <- repository.QueryResult{Error: err}
		} else {
			q.cache.Set(key, user, 10*time.Minute)
			result <- repository.QueryResult{Result: user}
		}
		close(result)
	}()
	return result
}

// GetByEmail return user by email
func (q *UserQueryCockroach) GetByEmail(email string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	key := "user-" + email
	go func() {
		var user domain.User
		if v := q.cache.Get(key); v != "" {
			err := json.Unmarshal([]byte(v), &user)
			if err != nil {
				result <- repository.QueryResult{Error: err}
				close(result)
			}
			result <- repository.QueryResult{Result: user}
			close(result)
			return
		}
		query := `
			SELECT id, name, username, email, password
			FROM users
			WHERE email=$1
		`
		err := q.db.QueryRow(
			query, email,
		).Scan(&user.ID, &user.Name, &user.Username, &user.Email, &user.Password)

		if err != nil {
			result <- repository.QueryResult{Error: err}
		} else {
			q.cache.Set(key, user, 10*time.Minute)
			result <- repository.QueryResult{Result: user}
		}
		close(result)
	}()
	return result
}

// GetByID return user by id
func (q *UserQueryCockroach) GetByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	key := "user-" + id.String()
	go func() {
		var user domain.User
		if v := q.cache.Get(key); v != "" {
			err := json.Unmarshal([]byte(v), &user)
			if err != nil {
				result <- repository.QueryResult{Error: err}
				close(result)
			}
			result <- repository.QueryResult{Result: user}
			close(result)
			return
		}
		query := `
			SELECT id, name, username, email, password
			FROM users
			WHERE id=$1
		`
		err := q.db.QueryRow(
			query, id,
		).Scan(&user.ID, &user.Name, &user.Username, &user.Email, &user.Password)

		if err != nil {
			result <- repository.QueryResult{Error: err}
		} else {
			q.cache.Set(key, user, 10*time.Minute)
			result <- repository.QueryResult{Result: user}
		}
		close(result)
	}()
	return result
}
