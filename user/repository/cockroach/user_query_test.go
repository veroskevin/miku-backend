package cockroach

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetByUsername(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	query := NewUserQueryCockroach(db)
	username := "username"

	t.Run("When user exists", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "name", "username", "email", "password"})
		rows = rows.AddRow(uuid.NewV4(), "name", "username", "email", "password")

		mock.ExpectQuery(`SELECT (.+) FROM users WHERE username=?`).
			WithArgs(username).
			WillReturnRows(rows)

		result := <-query.GetByUsername(username)
		assert.Nil(t, result.Error)
		assert.NotEmpty(t, result.Result)
		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("When user not exists", func(t *testing.T) {
		mock.ExpectQuery(`SELECT (.+) FROM users WHERE username=?`).
			WithArgs(username).
			WillReturnError(sql.ErrNoRows)

		result := <-query.GetByUsername(username)
		assert.Equal(t, result.Error, sql.ErrNoRows)
		assert.Empty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func TestGetByEmail(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	query := NewUserQueryCockroach(db)
	email := "email@gmail.com"

	t.Run("When user exists", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "name", "username", "email", "password"})
		rows = rows.AddRow(uuid.NewV4(), "name", "username", "email@gmail.com", "password")

		mock.ExpectQuery(`SELECT (.+) FROM users WHERE email=?`).
			WithArgs(email).
			WillReturnRows(rows)

		result := <-query.GetByEmail(email)
		assert.Nil(t, result.Error)
		assert.NotEmpty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("When user not exists", func(t *testing.T) {
		mock.ExpectQuery(`SELECT (.+) FROM users WHERE email=?`).
			WithArgs(email).
			WillReturnError(sql.ErrNoRows)

		result := <-query.GetByEmail(email)
		assert.Equal(t, result.Error, sql.ErrNoRows)
		assert.Empty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func TestGetByID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	query := NewUserQueryCockroach(db)
	id := uuid.NewV4()

	t.Run("When user exists", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "name", "username", "email", "password"})
		rows = rows.AddRow(id, "name", "username", "email", "password")

		mock.ExpectQuery(`SELECT (.+) FROM users WHERE id=?`).
			WithArgs(id).
			WillReturnRows(rows)

		result := <-query.GetByID(id)
		assert.Nil(t, result.Error)
		assert.NotEmpty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("When user not exists", func(t *testing.T) {
		mock.ExpectQuery(`SELECT (.+) FROM users WHERE id=?`).
			WithArgs(id).
			WillReturnError(sql.ErrNoRows)

		result := <-query.GetByID(id)
		assert.Equal(t, result.Error, sql.ErrNoRows)
		assert.Empty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}
