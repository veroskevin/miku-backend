package repository

import (
	"github.com/satori/go.uuid"
)

// QueryResult for query
type QueryResult struct {
	Result interface{}
	Error  error
}

// UserQuery interface
type UserQuery interface {
	GetByUsername(string) <-chan QueryResult
	GetByEmail(string) <-chan QueryResult
	GetByID(uuid.UUID) <-chan QueryResult
}
