package inmemory

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/repository"
	"gitlab.com/veroskevin/miku-backend/user/storage"
)

// UserRepositoryInMemory used to
// implement create and update
type UserRepositoryInMemory struct {
	Storage *storage.Storage
}

// NewUserRepositoryInMemory used to create a new UserRepositoryInMemory
func NewUserRepositoryInMemory(storage *storage.Storage) repository.UserRepository {
	return &UserRepositoryInMemory{
		Storage: storage,
	}
}

// Create used to save a new user to temporary storage
func (r *UserRepositoryInMemory) Create(user *domain.User) chan error {
	err := make(chan error)
	go func() {
		r.Storage.Users = append(r.Storage.Users, *user)
		err <- nil
		close(err)
	}()
	return err
}

// Update used to update existing user in temporary storage
func (r *UserRepositoryInMemory) Update(id uuid.UUID, user domain.User) chan error {
	err := make(chan error)
	go func() {
		for i := range r.Storage.Users {
			r.Storage.Users[i] = user
		}
		err <- nil
		close(err)
	}()
	return err
}
