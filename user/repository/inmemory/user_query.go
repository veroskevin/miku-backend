package inmemory

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/user/domain"
	"gitlab.com/veroskevin/miku-backend/user/repository"
	"gitlab.com/veroskevin/miku-backend/user/storage"
)

// UserQueryInMemory used to
// implement create and update
type UserQueryInMemory struct {
	Storage *storage.Storage
}

// NewUserQueryInMemory used to create a new UserQueryInMemory
func NewUserQueryInMemory(storage *storage.Storage) repository.UserQuery {
	return &UserQueryInMemory{
		Storage: storage,
	}
}

// GetByUsername used to get user by its username
func (q *UserQueryInMemory) GetByUsername(username string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		var user domain.User
		for _, v := range q.Storage.Users {
			if v.Username == username {
				user = v
			}
		}
		if (user.ID == uuid.NullUUID{}.UUID) {
			result <- repository.QueryResult{
				Error: domain.ErrUserNotFound,
			}
		} else {
			result <- repository.QueryResult{
				Result: user,
			}
		}
		close(result)
	}()

	return result
}

// GetByEmail used to get user by its email
func (q *UserQueryInMemory) GetByEmail(email string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		var user domain.User
		for _, v := range q.Storage.Users {
			if v.Email == email {
				user = v
			}
		}
		if (user.ID == uuid.NullUUID{}.UUID) {
			result <- repository.QueryResult{
				Error: domain.ErrUserNotFound,
			}
		} else {
			result <- repository.QueryResult{
				Result: user,
			}
		}
		close(result)
	}()

	return result
}

// GetByID used to get user by its id
func (q *UserQueryInMemory) GetByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		var user domain.User
		for _, v := range q.Storage.Users {
			if v.ID == id {
				user = v
			}
		}
		if (user.ID == uuid.NullUUID{}.UUID) {
			result <- repository.QueryResult{
				Error: domain.ErrUserNotFound,
			}
		} else {
			result <- repository.QueryResult{
				Result: user,
			}
		}
		close(result)
	}()

	return result
}
