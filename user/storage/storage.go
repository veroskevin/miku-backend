package storage

import (
	"gitlab.com/veroskevin/miku-backend/user/domain"
)

// Storage is to hold users data
type Storage struct {
	Users []domain.User
}

// NewStorage used to create a new storage
func NewStorage() *Storage {
	return &Storage{}
}
