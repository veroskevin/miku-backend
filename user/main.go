package main

import (
	"log"
	"net"

	"gitlab.com/veroskevin/miku-backend/user/config"

	pb "gitlab.com/veroskevin/miku-backend/user/proto"
	server "gitlab.com/veroskevin/miku-backend/user/server/grpc"
	"gitlab.com/veroskevin/miku-backend/user/service"
	"google.golang.org/grpc"
)

func main() {
	port := config.GrpcUserServicePort
	userService := service.NewUserService()
	userServer := server.NewUserServer(userService)

	grpcServer := grpc.NewServer()

	pb.RegisterUserServiceServer(grpcServer, userServer)

	log.Println("Starting RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", port, err)
	}

	log.Fatal("Server is listening", grpcServer.Serve(listen))
}
