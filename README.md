![alt text](Miku1.png "MIKU")

# MIKU
MIKU is the clone of kumparan.com

## Requirement
- Docker
- Docker compose


## Installation

- Install via docker-compose

        $ docker-compose up
        
- Application now running on port 8080

## Endpoints

     POST /upload  = uploading image endpoint
     POST /login   = login user endpoint
     POST /graphql = graphql endpoint
     GET /graphql  = graphql playground
