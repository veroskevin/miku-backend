package main

import (
	"gitlab.com/veroskevin/miku-backend/article/repository/cockroach"
	"log"
	"net"

	"gitlab.com/veroskevin/miku-backend/article/config"
	pb "gitlab.com/veroskevin/miku-backend/article/proto"
	server "gitlab.com/veroskevin/miku-backend/article/server/grpc"
	"gitlab.com/veroskevin/miku-backend/article/service"
	"google.golang.org/grpc"
)

func main() {
	db, err := config.InitDB()
	if err != nil {
		log.Fatal(err)
	}

	cache := config.NewRedis(config.RedisURL, config.RedisDB)
	articleQuery := cockroach.NewArticleQueryCockroach(db, cache)
	articleRepo := cockroach.NewArticleRepositoryCockroach(db, cache)
	categoryQuery := cockroach.NewCategoryQueryCockroach(db)
	categoryRepo := cockroach.NewCategoryRepositoryCockroach(db)
	as := service.NewArticleService(articleRepo, articleQuery, categoryQuery)
	cs := service.NewCategoryService(categoryRepo, categoryQuery)

	server := server.NewArticleServer(as, cs)
	grpcServer := grpc.NewServer()

	pb.RegisterArticleServiceServer(grpcServer, server)

	listen, err := net.Listen("tcp", config.GrpcArticleServicePort)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Starting RPC server at", config.GrpcArticleServicePort)
	grpcServer.Serve(listen)
}
