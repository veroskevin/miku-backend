module gitlab.com/veroskevin/miku-backend/article

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2
	github.com/gosimple/slug v1.6.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
	google.golang.org/grpc v1.22.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
