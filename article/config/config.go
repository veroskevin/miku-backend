package config

import (
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"io/ioutil"
	"log"
	"strings"
)

// Application configuration
var (
	GrpcArticleServicePort = ":9000"
	RedisURL               = "redis:6379"
	RedisDB                = 0
	DBSchemaPath           = "./database/schema.sql"
)

// InitDB schema
func InitDB() (*sql.DB, error) {
	db, err := sql.Open("postgres", "postgresql://root@cockroach:26257?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}
	_, err = db.Exec(`CREATE DATABASE IF NOT EXISTS article_service`)
	db.Close()

	db, err = sql.Open("postgres", "postgresql://root@cockroach:26257/article_service?sslmode=disable")
	err = db.Ping()
	if err != nil {
		log.Fatal("error connecting to the database: ", err)
	}

	src, err := ioutil.ReadFile(DBSchemaPath)
	schema := string(src)
	if err != nil {
		return nil, err
	}
	_, err = db.Exec(schema)
	if err != nil {
		return nil, err
	}

	categories := []string{
		domain.CategoryEntertainment,
		domain.CategoryNews,
		domain.CategoryOtomotif,
		domain.CategoryPolitic,
	}
	rows, _ := db.Query(`SELECT * FROM categories`)
	if !rows.Next() {
		for _, v := range categories {
			id := uuid.NewV4()
			slug := strings.ToLower(v)
			log.Println("inserting data:", id)
			_, err = db.Exec(`
				INSERT INTO categories(id, name, slug, user_id)
				VALUES($1, $2, $3, $4)
			`, id, v, slug, uuid.NullUUID{}.UUID)
			if err != nil {
				log.Println(err)
			}
		}
	}

	log.Println("InitDB: connected to database ...")
	return db, nil
}
