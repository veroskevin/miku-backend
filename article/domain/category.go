package domain

import (
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/veroskevin/miku-backend/article/proto"
	"strings"
)

// Default system category
const (
	CategoryNews          = "NEWS"
	CategoryPolitic       = "POLITIC"
	CategoryEntertainment = "ENTERTAINMENT"
	CategoryOtomotif      = "OTOMOTIF"
)

// Category represents the category
// of the article
type Category struct {
	ID     uuid.UUID
	Name   string
	Slug   string
	UserID uuid.UUID
}

// CategoryHelper is helper to category domain
type CategoryHelper interface {
	NameExists(string, uuid.UUID) error
}

// NewCategory used to create a new category
func NewCategory(h CategoryHelper, name, slug string, userID uuid.UUID) (*Category, error) {
	switch name {
	case CategoryEntertainment, CategoryNews, CategoryOtomotif, CategoryPolitic:
		return nil, ErrCategoryExists
	}
	err := h.NameExists(name, userID)
	if err != nil {
		return nil, err
	}

	slug = strings.ReplaceAll(slug, " ", "-")
	slug = strings.ToLower(slug)
	name = strings.ToUpper(name)
	return &Category{
		ID:     uuid.NewV4(),
		Name:   name,
		Slug:   slug,
		UserID: userID,
	}, nil
}

// Proto convert category struct to proto
func (c *Category) Proto() *pb.CategoryResponse {
	return &pb.CategoryResponse{
		Id:     c.ID.String(),
		Name:   c.Name,
		Slug:   c.Slug,
		UserId: c.UserID.String(),
	}
}
