package domain

import "errors"

var (
	// ErrTitleNotLongEnough = system found out that the title is not long enough
	ErrTitleNotLongEnough = errors.New("Title is not long enough")
	// ErrTitleTooLong = system found out that the title is too long
	ErrTitleTooLong = errors.New("Title is too long")
	// ErrCategoryExists = system found out that the category already exists
	ErrCategoryExists = errors.New("Category exists")
	// ErrSlugExists = system found out that the slug already exists
	ErrSlugExists = errors.New("Slug exists")
	// ErrCategoryNotFound = system found out that category not found
	ErrCategoryNotFound = errors.New("Category not found")
	// ErrArticleNotFound = system found out that article not found
	ErrArticleNotFound = errors.New("Article not found")
)
