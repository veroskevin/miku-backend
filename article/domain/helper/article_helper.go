package helper

import (
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// ArticleHelperImpl is category helper implementation
type ArticleHelperImpl struct {
	query repository.ArticleQuery
}

// NewArticleHelperImpl used to create a new article helper implementation
func NewArticleHelperImpl(query repository.ArticleQuery) domain.ArticleHelper {
	return &ArticleHelperImpl{
		query: query,
	}
}

// SlugExists used to check whether the slug is already exists in the storage
func (h *ArticleHelperImpl) SlugExists(slug string) error {
	result := h.query.GetBySlug(slug)
	if result.Error == nil {
		return domain.ErrSlugExists
	}

	return nil
}
