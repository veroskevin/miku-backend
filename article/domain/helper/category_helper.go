package helper

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// CategoryHelperImpl is category helper implementation
type CategoryHelperImpl struct {
	query repository.CategoryQuery
}

// NewCategoryHelperImpl used to create a new category helper implementation
func NewCategoryHelperImpl(query repository.CategoryQuery) domain.CategoryHelper {
	return &CategoryHelperImpl{
		query: query,
	}
}

// NameExists used to check whether the name is already exists in author's category and system category
func (h *CategoryHelperImpl) NameExists(name string, userID uuid.UUID) error {
	result := <-h.query.GetByNameAndUserID(name, userID)

	if result.Error == nil {
		return domain.ErrCategoryExists
	}

	return nil
}
