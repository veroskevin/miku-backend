package domain

import (
	"fmt"
	"strings"
	"time"
	s "github.com/gosimple/slug"
	pb "gitlab.com/veroskevin/miku-backend/article/proto"

	"github.com/golang/protobuf/ptypes"
	uuid "github.com/satori/go.uuid"
)

const (
	// StatusArticlePublished = the article has been published
	StatusArticlePublished = "PUBLISHED"
	// StatusArticleDrafted = the article is still a draft
	StatusArticleDrafted = "DRAFTED"
	// MinimumTitleLength = the minimum
	// title length the user must provide
	MinimumTitleLength = 10
	// MaximumTitleLength = the maximum
	// title length the user must provide
	MaximumTitleLength = 125
	// MinimumBodyLength = the minimum
	// body length the user must provide
)

// Article represents the article
type Article struct {
	ID         uuid.UUID
	ImageURL   string
	Title      string
	Body       string
	Slug       string
	Categories []Category
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Status     string
	UserID     uuid.UUID
}

// ArticleHelper is helper to article domain
type ArticleHelper interface {
	SlugExists(string) error
}

// NewArticle used to create a new article
func NewArticle(imageURL, title, body, slug string, userID uuid.UUID) *Article {
	if slug == "" {
		slug = generateSlug(title)
		slug = s.Make(slug)
	} else {
		slug = s.Make(title)
	}

	return &Article{
		ID:        uuid.NewV4(),
		ImageURL:  imageURL,
		Title:     title,
		Body:      body,
		Slug:      slug,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Status:    StatusArticleDrafted,
		UserID:    userID,
	}
}

func generateSlug(title string) string {
	uniqID := fmt.Sprint(time.Now().UnixNano())
	uniqID = uniqID[len(uniqID)/2:]
	parts := strings.Split(title, " ")
	parts = append(parts, uniqID)
	return strings.ToLower(strings.Join(parts, "-"))
}

// AddCategory used to add some category to an article
func (a *Article) AddCategory(category Category) error {
	found := false
	for _, v := range a.Categories {
		if v.ID == category.ID {
			found = true
		}
	}
	if !found {
		a.Categories = append(a.Categories, category)
	}
	return nil
}

// RemoveCategory from article
func (a *Article) RemoveCategory(id uuid.UUID) error {
	var categories []Category
	for _, v := range a.Categories {
		if v.ID != id {
			categories = append(categories, v)
		}
	}
	a.Categories = categories
	return nil
}

// Proto convert article struct to proto
func (a *Article) Proto() *pb.ArticleResponse {
	createdAt, _ := ptypes.TimestampProto(a.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(a.UpdatedAt)

	var categories []*pb.CategoryResponse
	for _, v := range a.Categories {
		categories = append(categories, v.Proto())
	}

	return &pb.ArticleResponse{
		Id:         a.ID.String(),
		ImageUrl:   a.ImageURL,
		Title:      a.Title,
		Body:       a.Body,
		Slug:       a.Slug,
		Status:     a.Status,
		CreatedAt:  createdAt,
		UpdatedAt:  updatedAt,
		UserId:     a.UserID.String(),
		Categories: categories,
	}
}

// ContainsCategory return true if category slug exists
func (a *Article) ContainsCategory(categorySlug string) bool {
	for _, v := range a.Categories {
		if strings.ToLower(categorySlug) == strings.ToLower(v.Slug) {
			return true
		}
	}
	return false
}

// Valid article
func (a *Article) Valid() error {
	title := strings.TrimSpace(a.Title)
	titleLength := len(title)
	if titleLength < MinimumTitleLength {
		return ErrTitleNotLongEnough
	} else if titleLength > MaximumTitleLength {
		return ErrTitleTooLong
	}
	return nil
}
