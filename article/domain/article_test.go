package domain

import (
	"github.com/satori/go.uuid"
	"testing"

	"github.com/stretchr/testify/assert"
)

func generateString(size int) string {
	var result string
	for i := 0; i < size; i++ {
		result += "a"
	}
	return result
}

func TestValidate(t *testing.T) {
	t.Run("Valid article", func(t *testing.T) {
		title := generateString(MinimumTitleLength)
		body := generateString(100)
		slug := "slug"
		userID := uuid.NewV4()

		article := NewArticle(slug, title, body, slug, userID)
		err := article.Valid()
		assert.Nil(t, err)
	})

	t.Run("Invalid article if title less than minimum lenght", func(t *testing.T) {
		title := generateString(MinimumTitleLength - 1)
		body := generateString(100)
		slug := "slug"
		userID := uuid.NewV4()

		article := NewArticle("", title, body, slug, userID)
		err := article.Valid()
		assert.Equal(t, ErrTitleNotLongEnough, err)
	})

	t.Run("Invalid article if title greater than maximum lenght", func(t *testing.T) {
		title := generateString(MaximumTitleLength + 1)
		body := generateString(100)
		slug := "slug"
		userID := uuid.NewV4()

		article := NewArticle("", title, body, slug, userID)
		err := article.Valid()
		assert.Equal(t, ErrTitleTooLong, err)
	})
}

func TestNewArticle(t *testing.T) {
	t.Run("Can create new article", func(t *testing.T) {
		title := generateString(MinimumTitleLength)
		body := generateString(100)
		slug := "haha"
		userID := uuid.NewV4()

		got := NewArticle(slug, title, body, slug, userID)
		assert.NotNil(t, got)
	})

	t.Run("Can generate slug if slug empty", func(t *testing.T) {
		title := generateString(MinimumTitleLength)
		body := generateString(100)
		slug := ""
		userID := uuid.NewV4()

		got := NewArticle(slug, title, body, slug, userID)
		assert.NotNil(t, got)
	})
}

func TestAddCategory(t *testing.T) {
	t.Run("When category not exist", func(t *testing.T) {
		category := Category{
			ID:     uuid.NewV4(),
			Name:   "name",
			Slug:   "Slug",
			UserID: uuid.NewV4(),
		}
		article := &Article{}
		err := article.AddCategory(category)
		assert.Nil(t, err)
	})

	t.Run("When category exist", func(t *testing.T) {
		category := Category{
			ID:     uuid.NewV4(),
			Name:   "name",
			Slug:   "Slug",
			UserID: uuid.NewV4(),
		}
		article := &Article{Categories: []Category{category}}
		err := article.AddCategory(category)
		assert.Nil(t, err)
	})
}

func TestRemoveCategory(t *testing.T) {
	t.Run("When category exist", func(t *testing.T) {
		category1 := Category{
			ID:     uuid.NewV4(),
			Name:   "name",
			Slug:   "Slug",
			UserID: uuid.NewV4(),
		}
		category2 := Category{
			ID:     uuid.NewV4(),
			Name:   "name",
			Slug:   "Slug",
			UserID: uuid.NewV4(),
		}
		article := &Article{
			Categories: []Category{category1, category2},
		}
		err := article.RemoveCategory(category2.ID)
		assert.Nil(t, err)
	})

	t.Run("When category not exist", func(t *testing.T) {
		article := &Article{}
		err := article.RemoveCategory(uuid.NewV4())
		assert.Nil(t, err)
	})

}

func TestProto(t *testing.T) {
	title := generateString(MinimumTitleLength)
	body := generateString(100)
	slug := "slug"
	userID := uuid.NewV4()

	article := NewArticle("", title, body, slug, userID)
	category := Category{
		ID:     uuid.NewV4(),
		Name:   "name",
		Slug:   "Slug",
		UserID: uuid.NewV4(),
	}
	article.AddCategory(category)

	got := article.Proto()
	assert.NotNil(t, got)
}

func TestContainsCategory(t *testing.T) {
	slug := "slug"
	t.Run("When category exists", func(t *testing.T) {
		category := Category{Slug: slug}
		article := &Article{
			Categories: []Category{category},
		}
		got := article.ContainsCategory(slug)
		assert.True(t, got)
	})

	t.Run("When category not exists", func(t *testing.T) {
		article := &Article{}
		got := article.ContainsCategory(slug)
		assert.False(t, got)
	})
}
