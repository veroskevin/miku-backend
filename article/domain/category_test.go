package domain

import (
	"github.com/satori/go.uuid"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/veroskevin/miku-backend/article/domain/mock"
)

func TestNewCategory(t *testing.T) {
	userID := uuid.NewV4()
	slug := "slug  Jaksd"

	t.Run("Can create category", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockCategoryHelper(controller)
		name := "category"
		m.EXPECT().NameExists(name, userID).Return(nil).AnyTimes()

		got, err := NewCategory(m, name, slug, userID)

		assert.NotNil(t, got)
		assert.Nil(t, err)
	})

	t.Run("Can't create category if category same as default caregory", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockCategoryHelper(controller)
		name := CategoryEntertainment
		m.EXPECT().NameExists(name, userID).Return(nil).AnyTimes()

		got, err := NewCategory(m, name, slug, userID)

		assert.Nil(t, got)
		assert.Equal(t, ErrCategoryExists, err)
	})

	t.Run("Can't create category if category exists in user category", func(t *testing.T) {
		controller := gomock.NewController(t)
		defer controller.Finish()
		m := mock.NewMockCategoryHelper(controller)
		name := "name"
		m.EXPECT().NameExists(name, userID).Return(ErrCategoryExists).AnyTimes()

		got, err := NewCategory(m, name, slug, userID)

		assert.Nil(t, got)
		assert.Equal(t, ErrCategoryExists, err)
	})
}
