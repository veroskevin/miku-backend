package repository

import (
	uuid "github.com/satori/go.uuid"
)

// QueryResult for query
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery interface
type ArticleQuery interface {
	GetByID(uuid.UUID) QueryResult
	GetBySlug(string) QueryResult
	GetPublished(PageInfo) QueryResult
	GetPublishedByUserID(uuid.UUID, PageInfo) QueryResult
	GetPublishedByCategorySlug(string, PageInfo) QueryResult
	GetPublishedByUserIDAndCategorySlug(uuid.UUID, string, PageInfo) QueryResult
	GetDraftedByUserID(uuid.UUID, PageInfo) QueryResult
	Search(string, PageInfo) QueryResult
	GetPublishedRelated(string, []string) QueryResult
}

// CategoryQuery interface
type CategoryQuery interface {
	GetByID(uuid.UUID) <-chan QueryResult
	GetByUserID(uuid.UUID, PageInfo) <-chan QueryResult
	GetByNameAndUserID(string, uuid.UUID) <-chan QueryResult
	GetDefault(info PageInfo) <-chan QueryResult
}
