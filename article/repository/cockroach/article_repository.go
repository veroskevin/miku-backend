package cockroach

import (
	"database/sql"
	"fmt"
	"gitlab.com/veroskevin/miku-backend/article/config"
	"time"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// ArticleRepositoryCockroach is article query implementation for cockroach db
type ArticleRepositoryCockroach struct {
	db    *sql.DB
	cache *config.Redis
}

// NewArticleRepositoryCockroach used to create a new ArticleRepositoryCockroach
func NewArticleRepositoryCockroach(db *sql.DB, cache *config.Redis) repository.ArticleRepository {
	return &ArticleRepositoryCockroach{
		db:    db,
		cache: cache,
	}
}

// Create article into database
func (r *ArticleRepositoryCockroach) Create(article *domain.Article) chan error {
	result := make(chan error)
	keySlug := "article-" + article.Slug
	keyID := fmt.Sprint("article-", article.ID)
	go func() {
		query := `
			INSERT INTO articles(id, image_url, title, body, slug, status, user_id, created_at, updated_at)
			VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)			
		`
		_, err := r.db.Exec(
			query, article.ID, article.ImageURL, article.Title, article.Body, article.Slug,
			article.Status, article.UserID, article.CreatedAt, article.UpdatedAt,
		)
		if err != nil {
			result <- err
		}
		close(result)
	}()

	go func(v domain.Article) {
		r.cache.Set(keySlug, v, 5*time.Minute)
		r.cache.Set(keyID, v, 5*time.Minute)
	}(*article)

	return result
}

// Update article in database
func (r *ArticleRepositoryCockroach) Update(id uuid.UUID, article domain.Article) chan error {
	result := make(chan error)
	key := "article-" + article.Slug
	go func() {
		query := `
			UPDATE articles SET image_url=$1, title=$2, body=$3, slug=$4, status=$5, updated_at=$6
			WHERE id=$7	
		`
		_, err := r.db.Exec(
			query, article.ImageURL, article.Title, article.Body,
			article.Slug, article.Status, time.Now(), id,
		)
		if err != nil {
			result <- err
			close(result)
			return
		}
		result <- nil
		close(result)
	}()

	go func(v domain.Article) {
		r.cache.Set(key, v, 5*time.Minute)
	}(article)
	return result
}

// AddCategory from article
func (r *ArticleRepositoryCockroach) AddCategory(articleID, categoryID uuid.UUID) chan error {
	result := make(chan error)
	go func() {
		q := `
			INSERT INTO article_categories(article_id, category_id)
			VALUES($1, $2)
		`
		_, err := r.db.Exec(q, articleID, categoryID)
		if err != nil {
			result <- err
			close(result)
			return
		}
		result <- nil
		close(result)
	}()
	return result
}

// RemoveCategory from article
func (r *ArticleRepositoryCockroach) RemoveCategory(articleID, categoryID uuid.UUID) chan error {
	result := make(chan error)
	go func() {
		q := `
			DELETE FROM article_categories ac
			WHERE ac.article_id=$1 AND ac.category_id=$2
		`
		_, err := r.db.Exec(q, articleID, categoryID)
		if err != nil {
			result <- err
			close(result)
			return
		}
		result <- nil
		close(result)
	}()
	return result
}
