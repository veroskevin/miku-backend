package cockroach

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.com/veroskevin/miku-backend/article/config"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// ArticleQueryCockroach is article query in memory impelemtation
type ArticleQueryCockroach struct {
	db    *sql.DB
	cache *config.Redis
}

// NewArticleQueryCockroach used to create a new article query in memory
func NewArticleQueryCockroach(db *sql.DB, cache *config.Redis) repository.ArticleQuery {
	return &ArticleQueryCockroach{
		db:    db,
		cache: cache,
	}
}

func parseRowsToArticle(rows *sql.Rows) ([]domain.Article, bool) {
	var result []domain.Article
	flag := make(map[uuid.UUID]bool)
	hasRows := false
	counter := 0
	for rows.Next() {
		hasRows = true
		var a domain.Article
		var c domain.Category
		rows.Scan(
			&a.ID, &a.ImageURL, &a.Title, &a.Body, &a.Slug,
			&a.Status, &a.UserID, &a.CreatedAt, &a.UpdatedAt,
			&c.ID, &c.Name, &c.Slug, &c.UserID,
		)
		if !flag[a.ID] {
			if (c.ID != uuid.NullUUID{}.UUID) {
				a.Categories = append(a.Categories, c)
			}
			result = append(result, a)
			counter++
			flag[a.ID] = true
		} else {
			if (c.ID != uuid.NullUUID{}.UUID) {
				result[counter-1].Categories = append(result[counter-1].Categories, c)
			}
		}
	}
	return result, hasRows
}

// GetByID used to get an article by its ID
func (q *ArticleQueryCockroach) GetByID(id uuid.UUID) repository.QueryResult {
	var article domain.Article

	if v := q.cache.Get(id.String()); v != "" {
		err := json.Unmarshal([]byte(v), &article)
		if err != nil {
			return repository.QueryResult{Error: err}
		}
		return repository.QueryResult{Result: article}
	}

	query := `
		SELECT 
			a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
			c.id, c.name, c.slug, c.user_id
		FROM articles a
		LEFT JOIN article_categories ac ON ac.article_id = a.id
		LEFT JOIN categories c ON c.id = ac.category_id
		WHERE a.id = $1
	`
	rows, err := q.db.Query(query, id)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	tmp, ok := parseRowsToArticle(rows)
	for _, a := range tmp {
		article = a
	}

	if !ok {
		return repository.QueryResult{Error: domain.ErrArticleNotFound}
	}

	func() {
		q.cache.Set(id.String(), article, 5*time.Minute)
	}()
	return repository.QueryResult{Result: article}
}

// GetBySlug used to get an article by its slug
func (q *ArticleQueryCockroach) GetBySlug(slug string) repository.QueryResult {
	var article domain.Article
	key := "article-" + slug
	if v := q.cache.Get(key); v != "" {
		err := json.Unmarshal([]byte(v), &article)
		if err != nil {
			return repository.QueryResult{Error: domain.ErrArticleNotFound}
		}
		return repository.QueryResult{Result: article}
	}
	query := `
		SELECT 
			a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
			c.id, c.name, c.slug, c.user_id
		FROM articles a
		LEFT JOIN article_categories ac ON ac.article_id = a.id
		LEFT JOIN categories c ON c.id = ac.category_id
		WHERE a.slug = $1
	`
	rows, err := q.db.Query(query, slug)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	tmp, ok := parseRowsToArticle(rows)
	for _, a := range tmp {
		article = a
	}

	if !ok {
		return repository.QueryResult{Error: domain.ErrArticleNotFound}
	}

	go func(v domain.Article) {
		q.cache.Set(key, article, 5*time.Minute)
	}(article)
	return repository.QueryResult{Result: article}
}

// GetPublished get all published articles
func (q *ArticleQueryCockroach) GetPublished(info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
		SELECT 
			a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
			c.id, c.name, c.slug, c.user_id
		FROM articles a
		LEFT JOIN article_categories ac ON ac.article_id = a.id
		LEFT JOIN categories c ON c.id = ac.category_id
		WHERE a.status = $1
		ORDER BY a.created_at DESC
		LIMIT $2 OFFSET $3
	`
	rows, err := q.db.Query(query, domain.StatusArticlePublished, info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	return repository.QueryResult{Result: articles}
}

// GetPublishedByUserID get all published articles by user id
func (q *ArticleQueryCockroach) GetPublishedByUserID(userID uuid.UUID, info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
		SELECT 
			a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
			c.id, c.name, c.slug, c.user_id
		FROM articles a
		LEFT JOIN article_categories ac ON ac.article_id = a.id
		LEFT JOIN categories c ON c.id = ac.category_id
		WHERE a.status = $1 AND a.user_id = $2
		ORDER BY a.created_at DESC
		LIMIT $3 OFFSET $4
	`
	rows, err := q.db.Query(query, domain.StatusArticlePublished, userID, info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	return repository.QueryResult{Result: articles}
}

// GetPublishedByCategorySlug return articles by category slug
func (q *ArticleQueryCockroach) GetPublishedByCategorySlug(categorySlug string, info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
			SELECT 
				a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
				c.id, c.name, c.slug, c.user_id
			FROM articles a
			LEFT JOIN article_categories ac ON ac.article_id = a.id
			LEFT JOIN categories c ON c.id = ac.category_id
			WHERE a.status = $1 AND LOWER(c.slug) = LOWER($2)
			ORDER BY a.created_at DESC
			LIMIT $3 OFFSET $4
		`
	rows, err := q.db.Query(query, domain.StatusArticlePublished, categorySlug, info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	return repository.QueryResult{Result: articles}
}

// GetPublishedByUserIDAndCategorySlug return published articles
func (q *ArticleQueryCockroach) GetPublishedByUserIDAndCategorySlug(userID uuid.UUID, categorySlug string, info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
			SELECT 
				a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
				c.id, c.name, c.slug, c.user_id
			FROM articles a
			LEFT JOIN article_categories ac ON ac.article_id = a.id
			LEFT JOIN categories c ON c.id = ac.category_id
			WHERE a.status = $1 AND a.user_id = $2 AND LOWER(c.slug) = LOWER($3)
			ORDER BY a.created_at DESC
			LIMIT $4 OFFSET $5
		`
	rows, err := q.db.Query(query, domain.StatusArticlePublished, userID, categorySlug, info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}
	return repository.QueryResult{Result: articles}
}

// GetDraftedByUserID return all drafted articles by user id
func (q *ArticleQueryCockroach) GetDraftedByUserID(userID uuid.UUID, info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
			SELECT 
				a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
				c.id, c.name, c.slug, c.user_id
			FROM articles a
			LEFT JOIN article_categories ac ON ac.article_id = a.id
			LEFT JOIN categories c ON c.id = ac.category_id
			WHERE a.status = $1 AND a.user_id = $2
			ORDER BY a.created_at DESC
			LIMIT $3 OFFSET $4
		`
	rows, err := q.db.Query(query, domain.StatusArticleDrafted, userID, info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	return repository.QueryResult{Result: articles}
}

// Search article
func (q *ArticleQueryCockroach) Search(search string, info repository.PageInfo) repository.QueryResult {
	var articles []domain.Article
	query := `
			SELECT 
				a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
				c.id, c.name, c.slug, c.user_id
			FROM articles a
			LEFT JOIN article_categories ac ON ac.article_id = a.id
			LEFT JOIN categories c ON c.id = ac.category_id
			WHERE a.status = $1 AND LOWER(a.title) LIKE $2
			ORDER BY a.created_at DESC
			LIMIT $3 OFFSET $4
		`
	rows, err := q.db.Query(query, domain.StatusArticlePublished, "%"+strings.ToLower(search)+"%", info.Size, info.Cursor)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	return repository.QueryResult{Result: articles}
}

// GetPublishedRelated used to get articles that related to the article being seen
func (q *ArticleQueryCockroach) GetPublishedRelated(title string, categories []string) repository.QueryResult {
	var articles []domain.Article
	for i, v := range categories {
		categories[i] = fmt.Sprintf("'%s'", v)
	}
	data := strings.Join(categories, ", ")
	query := `
			SELECT 
				a.id, a.image_url, a.title, a.body, a.slug, a.status, a.user_id, a.created_at, a.updated_at,
				c.id, c.name, c.slug, c.user_id
			FROM articles a
			LEFT JOIN article_categories ac ON ac.article_id = a.id
			LEFT JOIN categories c ON c.id = ac.category_id
			WHERE a.status = $1 AND LOWER(c.slug) IN (` + data +
		`) ORDER BY a.created_at DESC
	`
	rows, err := q.db.Query(query, domain.StatusArticlePublished)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	defer rows.Close()

	articles, _ = parseRowsToArticle(rows)
	//for _, a := range tmp {
	//	articles = append(articles, a)
	//}

	if len(articles) == 0 {
		info := repository.PageInfo{Size: 5, Cursor: "0"}
		return q.GetPublished(info)
	}
	return repository.QueryResult{Result: articles}
}
