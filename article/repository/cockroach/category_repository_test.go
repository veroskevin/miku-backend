package cockroach

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"testing"
)

func TestCreate(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	repo := NewCategoryRepositoryCockroach(db)

	id := uuid.NewV4()
	name := "name"
	slug := "slug"
	userID := uuid.NewV4()

	category := &domain.Category{
		ID:     id,
		Name:   name,
		Slug:   slug,
		UserID: userID,
	}

	t.Run("Insert success", func(t *testing.T) {
		mock.ExpectExec(`INSERT INTO categories`).
			WithArgs(id, name, slug, userID).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := <-repo.Create(category)
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("Insert fail", func(t *testing.T) {
		mock.ExpectExec(`INSERT INTO categories`).
			WithArgs(id, name, slug, userID).
			WillReturnError(domain.ErrCategoryExists)

		err := <-repo.Create(category)
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

}
