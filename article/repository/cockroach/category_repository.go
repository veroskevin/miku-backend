package cockroach

import (
	"database/sql"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// CategoryRepositoryCockroach is category repository in cockroachdb implementation
type CategoryRepositoryCockroach struct {
	db *sql.DB
}

// NewCategoryRepositoryCockroach return new category repository in cockroachdb implementation
func NewCategoryRepositoryCockroach(db *sql.DB) repository.CategoryRepository {
	return &CategoryRepositoryCockroach{
		db: db,
	}
}

// Create is to create category into database
func (r *CategoryRepositoryCockroach) Create(category *domain.Category) chan error {
	result := make(chan error)
	go func() {
		query := `
			INSERT INTO categories(id, name, slug, user_id)
			VALUES($1, $2, $3, $4)			
		`
		_, err := r.db.Exec(
			query, category.ID, category.Name, category.Slug, category.UserID,
		)
		if err != nil {
			result <- err
		}
		close(result)
	}()

	return result
}
