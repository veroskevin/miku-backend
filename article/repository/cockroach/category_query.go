package cockroach

import (
	"database/sql"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/repository"
	"log"
)

// CategoryQueryCockroach is category query cockroach implementation
type CategoryQueryCockroach struct {
	db *sql.DB
}

// NewCategoryQueryCockroach return new category query cockroach implementation
func NewCategoryQueryCockroach(db *sql.DB) repository.CategoryQuery {
	return &CategoryQueryCockroach{
		db: db,
	}
}

// GetByID return category by id
func (q *CategoryQueryCockroach) GetByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		var category domain.Category
		query := `
			SELECT id, name, slug, user_id
			FROM categories
			WHERE id=$1
		`
		err := q.db.QueryRow(
			query, id,
		).Scan(&category.ID, &category.Name, &category.Slug, &category.UserID)

		if err == sql.ErrNoRows {
			result <- repository.QueryResult{Error: domain.ErrCategoryNotFound}
		} else {
			result <- repository.QueryResult{Result: category}
		}
		close(result)
	}()
	return result
}

// GetByUserID return categories by user id
func (q *CategoryQueryCockroach) GetByUserID(userID uuid.UUID, info repository.PageInfo) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		var category []domain.Category
		query := `
			SELECT id, name, slug, user_id
			FROM categories
			WHERE (user_id = $1 OR user_id = $2)
			LIMIT $3 OFFSET $4
		`
		rows, err := q.db.Query(query, userID, uuid.NullUUID{}.UUID, info.Size, info.Cursor)
		if err != nil {
			result <- repository.QueryResult{Error: err}
			close(result)
			return
		}
		defer rows.Close()

		for rows.Next() {
			var c domain.Category
			rows.Scan(&c.ID, &c.Name, &c.Slug, &c.UserID)
			category = append(category, c)
		}
		result <- repository.QueryResult{Result: category}
		close(result)
	}()
	return result
}

// GetByNameAndUserID return category by name and user id
func (q *CategoryQueryCockroach) GetByNameAndUserID(name string, userID uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		var category domain.Category
		query := `
			SELECT id, name, slug, user_id
			FROM categories
			WHERE LOWER(name) = LOWER($1) AND user_id = $2
		`
		err := q.db.QueryRow(
			query, name, userID,
		).Scan(&category.ID, &category.Name, &category.Slug, &category.UserID)

		if err != nil {
			result <- repository.QueryResult{Error: err}
		} else {
			result <- repository.QueryResult{Result: category}
		}
		close(result)
	}()
	return result
}

// GetDefault return default categories
func (q *CategoryQueryCockroach) GetDefault(info repository.PageInfo) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)
	go func() {
		var category []domain.Category
		query := `
			SELECT id, name, slug, user_id
			FROM categories
			WHERE user_id=$1
			LIMIT $2 OFFSET $3
		`
		rows, err := q.db.Query(query, uuid.NullUUID{}.UUID, info.Size, info.Cursor)
		if err != nil {
			result <- repository.QueryResult{Error: err}
			close(result)
			return
		}
		defer rows.Close()

		for rows.Next() {
			var c domain.Category
			err := rows.Scan(&c.ID, &c.Name, &c.Slug, &c.UserID)
			if err != nil {
				log.Println("GetDefault:", err)
			}
			category = append(category, c)
		}
		result <- repository.QueryResult{Result: category}
		close(result)
	}()
	return result
}
