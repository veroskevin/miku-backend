package repository

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
)

// ArticleRepository used to hold
// functions to be implemented
type ArticleRepository interface {
	Create(*domain.Article) chan error
	Update(uuid.UUID, domain.Article) chan error
	AddCategory(uuid.UUID, uuid.UUID) chan error
	RemoveCategory(uuid.UUID, uuid.UUID) chan error
}

// CategoryRepository used to hold
// functions to be implemented
type CategoryRepository interface {
	Create(*domain.Category) chan error
}

type PageInfo struct {
	Size   int32
	Cursor string
}
