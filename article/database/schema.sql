CREATE TABLE IF NOT EXISTS articles(
    id UUID PRIMARY KEY NOT NULL,
    image_url VARCHAR(255),
    title VARCHAR(125),
    body TEXT,
    slug  VARCHAR(255) UNIQUE,
    status VARCHAR(20),
    user_id UUID,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE INDEX IF NOT EXISTS article_user_id_idx ON articles(user_id);
CREATE INDEX IF NOT EXISTS article_status_idx ON articles(status);

CREATE TABLE IF NOT EXISTS categories(
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(50),
    slug VARCHAR(125) UNIQUE,
    user_id UUID
);

CREATE INDEX IF NOT EXISTS category_user_id_idx ON categories(user_id);

CREATE TABLE if not exists article_categories(
    article_id UUID REFERENCES articles(id) ON DELETE CASCADE,
    category_id UUID REFERENCES categories(id) ON DELETE CASCADE
);