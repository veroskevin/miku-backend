package service

import (
	"github.com/gosimple/slug"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/domain/helper"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// ArticleService is service for user domain
type ArticleService struct {
	categoryQuery     repository.CategoryQuery
	articleQuery      repository.ArticleQuery
	articleRepository repository.ArticleRepository
}

// NewArticleService return new ArticleService
func NewArticleService(
	articleRepo repository.ArticleRepository, articleQuery repository.ArticleQuery,
	categoryQuery repository.CategoryQuery) *ArticleService {
	return &ArticleService{
		articleRepository: articleRepo,
		articleQuery:      articleQuery,
		categoryQuery:     categoryQuery,
	}
}

// GetByID by return article by id
func (s *ArticleService) GetByID(id uuid.UUID) (domain.Article, error) {
	result := s.articleQuery.GetByID(id)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}

	article := result.Result.(domain.Article)
	return article, nil
}

// GetBySlug used to get an article by its slug
func (s *ArticleService) GetBySlug(slug string) (domain.Article, error) {
	result := s.articleQuery.GetBySlug(slug)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}

	article := result.Result.(domain.Article)
	return article, nil
}

// GetPublished used to get an array of all published article
func (s *ArticleService) GetPublished(info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.GetPublished(info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// GetPublishedByUserID used to get an array of all published article by its user id
func (s *ArticleService) GetPublishedByUserID(userID uuid.UUID, info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.GetPublishedByUserID(userID, info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// GetPublishedByCategorySlug return published article by category slug
func (s *ArticleService) GetPublishedByCategorySlug(categorySlug string, info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.GetPublishedByCategorySlug(categorySlug, info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// GetPublishedByUserIDAndCategorySlug return articles by user id and category slug
func (s *ArticleService) GetPublishedByUserIDAndCategorySlug(userID uuid.UUID, categorySlug string, info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.GetPublishedByUserIDAndCategorySlug(userID, categorySlug, info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// GetDraftedByUserID return drafted article by user id
func (s *ArticleService) GetDraftedByUserID(userID uuid.UUID, info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.GetDraftedByUserID(userID, info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// CreateArticle used to create a new user
func (s *ArticleService) CreateArticle(imageURL, title, body, slug string, userID uuid.UUID) (*domain.Article, error) {
	h := helper.NewArticleHelperImpl(s.articleQuery)
	article := domain.NewArticle(imageURL, title, body, slug, userID)
	err := article.Valid()
	if err != nil {
		return nil, err
	}
	err = h.SlugExists(slug)
	if err != nil {
		return nil, err
	}

	err = <-s.articleRepository.Create(article)
	if err != nil {
		return nil, err
	}

	return article, nil
}

// AddCategory is to add category to category
func (s *ArticleService) AddCategory(articleID, categoryID uuid.UUID) (domain.Article, error) {
	result := s.articleQuery.GetByID(articleID)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}
	article := result.Result.(domain.Article)

	result = <-s.categoryQuery.GetByID(categoryID)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}
	category := result.Result.(domain.Category)
	article.AddCategory(category)
	err := <-s.articleRepository.AddCategory(article.ID, category.ID)
	if err != nil {
		return domain.Article{}, err
	}
	return article, nil
}

// PublishArticle used to change the status of the article to published
func (s *ArticleService) PublishArticle(id uuid.UUID) (domain.Article, error) {
	result := s.articleQuery.GetByID(id)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}

	article := result.Result.(domain.Article)
	if article.Status == domain.StatusArticlePublished {
		return article, nil
	}

	article.Status = domain.StatusArticlePublished
	err := <-s.articleRepository.Update(id, article)
	if err != nil {
		return domain.Article{}, err
	}

	return article, nil
}

// Search article based on query
func (s *ArticleService) Search(query string, info repository.PageInfo) ([]domain.Article, error) {
	result := s.articleQuery.Search(query, info)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// GetPublishedRelatedArticles used to get articles that related to the article being seen
func (s *ArticleService) GetPublishedRelatedArticles(title string, category []string) ([]domain.Article, error) {
	result := s.articleQuery.GetPublishedRelated(title, category)
	if result.Error != nil {
		return nil, result.Error
	}

	article := result.Result.([]domain.Article)
	return article, nil
}

// UpdateArticle is to update article in database
func (s *ArticleService) UpdateArticle(id uuid.UUID, article *domain.Article) error {
	h := helper.NewArticleHelperImpl(s.articleQuery)
	newArticle, err := s.GetByID(id)
	if err != nil {
		return err
	}
	newArticle.ImageURL = article.ImageURL
	newArticle.Title = article.Title
	newArticle.Body = article.Body
	if newArticle.Slug != article.Slug {
		newArticle.Slug = slug.Make(article.Slug)
		err := h.SlugExists(newArticle.Slug)
		if err != nil {
			return err
		}
	}
	err = newArticle.Valid()
	if err != nil {
		return err
	}
	err = <-s.articleRepository.Update(id, newArticle)
	if err != nil {
		return err
	}
	*article = newArticle
	return nil
}

// RemoveCategory is to remove category from article
func (s *ArticleService) RemoveCategory(articleID, categoryID uuid.UUID) (domain.Article, error) {
	result := s.articleQuery.GetByID(articleID)
	if result.Error != nil {
		return domain.Article{}, result.Error
	}
	article := result.Result.(domain.Article)
	article.RemoveCategory(categoryID)

	err := <-s.articleRepository.RemoveCategory(article.ID, categoryID)
	if err != nil {
		return domain.Article{}, err
	}
	return article, nil
}
