package service

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
	"gitlab.com/veroskevin/miku-backend/article/domain/helper"
	"gitlab.com/veroskevin/miku-backend/article/repository"
)

// CategoryService is service for category domain
type CategoryService struct {
	query      repository.CategoryQuery
	repository repository.CategoryRepository
}

// NewCategoryService used to create a new category service
func NewCategoryService(repo repository.CategoryRepository, query repository.CategoryQuery) *CategoryService {
	return &CategoryService{
		repository: repo,
		query:      query,
	}
}

// CreateCategory used to create a new user
func (s *CategoryService) CreateCategory(name, slug string, userID uuid.UUID) (domain.Category, error) {
	helper := helper.NewCategoryHelperImpl(s.query)
	user, err := domain.NewCategory(helper, name, slug, userID)
	if err != nil {
		return domain.Category{}, err
	}

	err = <-s.repository.Create(user)
	if err != nil {
		return domain.Category{}, err
	}

	return *user, nil
}

// GetByID used to get category by id
func (s *CategoryService) GetByID(id uuid.UUID) (domain.Category, error) {
	result := <-s.query.GetByID(id)
	if result.Error != nil {
		return domain.Category{}, result.Error
	}

	return result.Result.(domain.Category), nil
}

// GetByUserID used to create a new user
func (s *CategoryService) GetByUserID(userID uuid.UUID, info repository.PageInfo) ([]domain.Category, error) {
	result := <-s.query.GetByUserID(userID, info)
	if result.Error != nil {
		return nil, result.Error
	}

	return result.Result.([]domain.Category), nil
}

// GetDefault all categories
func (s *CategoryService) GetDefault(info repository.PageInfo) ([]domain.Category, error) {
	result := <-s.query.GetDefault(info)
	if result.Error != nil {
		return nil, result.Error
	}

	return result.Result.([]domain.Category), nil
}
