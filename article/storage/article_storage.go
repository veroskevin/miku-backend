package storage

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"
)

// ArticleStorage is to hold articles data
type ArticleStorage struct {
	Articles   []domain.Article
	Categories []domain.Category
}

// NewArticleStorage used to create a new storage
func NewArticleStorage() *ArticleStorage {
	var categories []domain.Category
	categories = append(categories, domain.Category{
		ID:     uuid.NewV4(),
		Name:   domain.CategoryEntertainment,
		Slug:   domain.CategoryEntertainment,
		UserID: uuid.NullUUID{}.UUID,
	})
	categories = append(categories, domain.Category{
		ID:     uuid.NewV4(),
		Name:   domain.CategoryNews,
		Slug:   domain.CategoryNews,
		UserID: uuid.NullUUID{}.UUID,
	})
	categories = append(categories, domain.Category{
		ID:     uuid.NewV4(),
		Name:   domain.CategoryPolitic,
		Slug:   domain.CategoryPolitic,
		UserID: uuid.NullUUID{}.UUID,
	})
	categories = append(categories, domain.Category{
		ID:     uuid.NewV4(),
		Name:   domain.CategoryOtomotif,
		Slug:   domain.CategoryOtomotif,
		UserID: uuid.NullUUID{}.UUID,
	})
	return &ArticleStorage{
		Categories: categories,
	}
}
