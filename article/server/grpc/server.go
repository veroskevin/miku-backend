package grpc

import (
	"context"
	"gitlab.com/veroskevin/miku-backend/article/repository"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/article/domain"

	_ "github.com/lib/pq"
	pb "gitlab.com/veroskevin/miku-backend/article/proto"
	"gitlab.com/veroskevin/miku-backend/article/service"
)

// ArticleServer is GRPC article service server implementation
type ArticleServer struct {
	articleService  *service.ArticleService
	categoryService *service.CategoryService
}

// NewArticleServer return new article server
func NewArticleServer(as *service.ArticleService, cs *service.CategoryService) pb.ArticleServiceServer {
	return &ArticleServer{
		articleService:  as,
		categoryService: cs,
	}
}

// CreateArticle is to create article
func (s *ArticleServer) CreateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	id, _ := uuid.FromString(param.UserId)
	article, err := s.articleService.CreateArticle(param.ImageUrl, param.Title, param.Body, param.Slug, id)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// UpdateArticle is to create article
func (s *ArticleServer) UpdateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	id, _ := uuid.FromString(param.Id)
	article := domain.NewArticle(
		param.ImageUrl, param.Title,
		param.Body, param.Slug, uuid.NullUUID{}.UUID,
	)

	err := s.articleService.UpdateArticle(id, article)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// GetArticleByID used to get an article by its ID
func (s *ArticleServer) GetArticleByID(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	id, _ := uuid.FromString(param.Id)
	article, err := s.articleService.GetByID(id)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// GetArticleBySlug used to get an article by its slug
func (s *ArticleServer) GetArticleBySlug(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	article, err := s.articleService.GetBySlug(param.Slug)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// GetPublishedArticles used to get an array of published article
func (s *ArticleServer) GetPublishedArticles(ctx context.Context, param *pb.ArticleRequest) (*pb.ListArticle, error) {
	var listArticles pb.ListArticle
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.GetPublished(info)
	if err != nil {
		return nil, err
	}
	for _, v := range articles {
		listArticles.Data = append(listArticles.Data, v.Proto())
	}

	return &listArticles, nil
}

// GetPublishedArticlesByUserID return published article by user id
func (s *ArticleServer) GetPublishedArticlesByUserID(ctx context.Context, param *pb.ArticleRequest) (*pb.ListArticle, error) {
	var result pb.ListArticle
	id, _ := uuid.FromString(param.UserId)
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.GetPublishedByUserID(id, info)
	if err != nil {
		return nil, err
	}
	for _, v := range articles {
		result.Data = append(result.Data, v.Proto())
	}

	return &result, nil
}

// GetPublishedArticlesByCategorySlug used to get an array of published article by its category
func (s *ArticleServer) GetPublishedArticlesByCategorySlug(ctx context.Context, param *pb.ArticleRequest) (*pb.ListArticle, error) {
	var listArticles pb.ListArticle
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.GetPublishedByCategorySlug(param.CategorySlug, info)
	if err != nil {
		return nil, err
	}
	for _, v := range articles {
		listArticles.Data = append(listArticles.Data, v.Proto())
	}

	return &listArticles, nil
}

// GetPublishedArticlesByUserIDAndCategorySlug used to get an array of published article by user id and category slug
func (s *ArticleServer) GetPublishedArticlesByUserIDAndCategorySlug(ctx context.Context, param *pb.ArticleRequest) (*pb.ListArticle, error) {
	var listArticles pb.ListArticle
	id, _ := uuid.FromString(param.UserId)
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.GetPublishedByUserIDAndCategorySlug(id, param.CategorySlug, info)
	if err != nil {
		return nil, err
	}
	for _, v := range articles {
		listArticles.Data = append(listArticles.Data, v.Proto())
	}

	return &listArticles, nil
}

// GetDraftedArticlesByUserID used to get an array of drafted article
func (s *ArticleServer) GetDraftedArticlesByUserID(ctx context.Context, param *pb.ArticleRequest) (*pb.ListArticle, error) {
	var listArticles pb.ListArticle
	id, _ := uuid.FromString(param.UserId)
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.GetDraftedByUserID(id, info)
	if err != nil {
		return nil, err
	}
	for _, v := range articles {
		listArticles.Data = append(listArticles.Data, v.Proto())
	}

	return &listArticles, nil
}

// PublishArticle used to change the status of the article to published
func (s *ArticleServer) PublishArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	id, _ := uuid.FromString(param.Id)
	article, err := s.articleService.PublishArticle(id)
	if err != nil {
		return nil, err
	}
	return article.Proto(), nil
}

// AddCategory is to add category to article
func (s *ArticleServer) AddCategory(ctx context.Context, param *pb.ArticleCategoryRequest) (*pb.ArticleResponse, error) {
	articleID, _ := uuid.FromString(param.ArticleId)
	categoryID, _ := uuid.FromString(param.CategoryId)
	article, err := s.articleService.AddCategory(articleID, categoryID)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// RemoveCategory is to remove category from article
func (s *ArticleServer) RemoveCategory(ctx context.Context, param *pb.ArticleCategoryRequest) (*pb.ArticleResponse, error) {
	articleID, _ := uuid.FromString(param.ArticleId)
	categoryID, _ := uuid.FromString(param.CategoryId)
	article, err := s.articleService.RemoveCategory(articleID, categoryID)
	if err != nil {
		return nil, err
	}

	return article.Proto(), nil
}

// CreateCategory is to create category
func (s *ArticleServer) CreateCategory(ctx context.Context, param *pb.CategoryRequest) (*pb.CategoryResponse, error) {
	id, _ := uuid.FromString(param.UserId)
	category, err := s.categoryService.CreateCategory(param.Name, param.Slug, id)
	if err != nil {
		return nil, err
	}

	return category.Proto(), nil
}

// GetCategoryByID is to get category by id
func (s *ArticleServer) GetCategoryByID(ctx context.Context, param *pb.CategoryRequest) (*pb.CategoryResponse, error) {
	id, _ := uuid.FromString(param.UserId)
	article, err := s.categoryService.GetByID(id)
	if err != nil {
		return nil, err
	}
	return article.Proto(), nil
}

// GetCategoriesByUserID return categories by user id
func (s *ArticleServer) GetCategoriesByUserID(ctx context.Context, param *pb.CategoryRequest) (*pb.ListCategory, error) {
	id, _ := uuid.FromString(param.UserId)
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.categoryService.GetByUserID(id, info)
	if err != nil {
		return nil, err
	}
	result := make([]*pb.CategoryResponse, len(articles))
	for i, v := range articles {
		result[i] = v.Proto()
	}

	return &pb.ListCategory{
		Data: result,
	}, nil
}

// GetDefaultCategories return default categories
func (s *ArticleServer) GetDefaultCategories(ctx context.Context, param *pb.CategoryRequest) (*pb.ListCategory, error) {
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.categoryService.GetDefault(info)
	if err != nil {
		return nil, err
	}
	result := make([]*pb.CategoryResponse, len(articles))
	for i, v := range articles {
		result[i] = v.Proto()
	}

	return &pb.ListCategory{
		Data: result,
	}, nil
}

// SearchArticle return article by searching query
func (s *ArticleServer) SearchArticle(ctx context.Context, param *pb.SearchRequest) (*pb.ListArticle, error) {
	info := repository.PageInfo{
		Size:   param.First,
		Cursor: param.After,
	}
	articles, err := s.articleService.Search(param.Query, info)
	if err != nil {
		return nil, err
	}
	result := make([]*pb.ArticleResponse, len(articles))
	for i, v := range articles {
		result[i] = v.Proto()
	}

	return &pb.ListArticle{
		Data: result,
	}, nil
}

// GetPublishedRelatedArticles returns articles related to the article being seen
func (s *ArticleServer) GetPublishedRelatedArticles(ctx context.Context, param *pb.RelatedArticleRequest) (*pb.ListArticle, error) {
	articles, err := s.articleService.GetPublishedRelatedArticles(param.Title, param.CategorySlug)
	if err != nil {
		return nil, err
	}
	result := make([]*pb.ArticleResponse, len(articles))
	for i, v := range articles {
		result[i] = v.Proto()
	}

	return &pb.ListArticle{
		Data: result,
	}, nil
}
