package repository

import (
	"github.com/satori/go.uuid"
)

// QueryResult used to hold results and error of a query
type QueryResult struct {
	Result interface{}
	Error  error
}

// CommentQuery interface
type CommentQuery interface {
	GetByArticleID(uuid.UUID, PageInfo) <-chan QueryResult
	CountByArticleID(uuid.UUID) QueryResult
}
