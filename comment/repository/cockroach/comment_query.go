package cockroach

import (
	"database/sql"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/comment/config"
	"gitlab.com/veroskevin/miku-backend/comment/domain"
	"gitlab.com/veroskevin/miku-backend/comment/repository"
)

// CommentQueryCockroach used to implement
// some functions in query interface
type CommentQueryCockroach struct {
	db    *sql.DB
	cache *config.Redis
}

// NewCommentQueryCockroach used to create a new comment query in memory
func NewCommentQueryCockroach(db *sql.DB, cache *config.Redis) repository.CommentQuery {
	return &CommentQueryCockroach{
		db:    db,
		cache: cache,
	}
}

// GetByArticleID used to get comments with articleID as provided
func (q *CommentQueryCockroach) GetByArticleID(articleID uuid.UUID, info repository.PageInfo) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		var comments []domain.Comment
		query := `
			SELECT id, body, article_id, user_id, created_at, updated_at
			FROM comments
			WHERE article_id = $1
			ORDER BY created_at DESC 
			LIMIT $2 OFFSET $3
		`
		rows, err := q.db.Query(query, articleID, info.Size, info.Cursor)
		if err != nil {
			result <- repository.QueryResult{Error: err}
			close(result)
			return
		}

		for rows.Next() {
			var c domain.Comment
			err := rows.Scan(&c.ID, &c.Body, &c.ArticleID, &c.UserID, &c.CreatedAt, &c.UpdatedAt)
			if err != nil {
				result <- repository.QueryResult{Error: err}
				close(result)
				return
			}
			comments = append(comments, c)
		}
		result <- repository.QueryResult{Result: comments}
		close(result)
	}()

	return result
}

func (q *CommentQueryCockroach) CountByArticleID(articleID uuid.UUID) repository.QueryResult {
	var count int32
	query := `SELECT COUNT(*) FROM comments WHERE article_id = $1`

	err := q.db.QueryRow(query, articleID).Scan(&count)
	if err != nil {
		return repository.QueryResult{Error: err}
	}
	return repository.QueryResult{Result: count}
}