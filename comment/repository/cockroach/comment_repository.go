package cockroach

import (
	"database/sql"
	"gitlab.com/veroskevin/miku-backend/comment/config"
	"gitlab.com/veroskevin/miku-backend/comment/domain"
	"gitlab.com/veroskevin/miku-backend/comment/repository"
)

// CommentRepositoryCockroach used to
// implement create
type CommentRepositoryCockroach struct {
	db    *sql.DB
	cache *config.Redis
}

// NewCommentRepositoryCockroach used to create a new CommentRepositoryCockroach
func NewCommentRepositoryCockroach(db *sql.DB, cache *config.Redis) repository.CommentRepository {
	return &CommentRepositoryCockroach{
		db:    db,
		cache: cache,
	}
}

// Create used to save a new comment to temporary storage
func (r *CommentRepositoryCockroach) Create(comment *domain.Comment) error {
	query := `
		INSERT INTO comments(id, body, user_id, article_id, created_at, updated_at)
		VALUES($1, $2, $3, $4, $5, $6)`
	_, err := r.db.Exec(
		query, comment.ID, comment.Body, comment.UserID,
		comment.ArticleID, comment.CreatedAt, comment.UpdatedAt,
	)
	if err != nil {
		return err
	}
	return nil
}
