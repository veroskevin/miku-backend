package cockroach

import (
	"database/sql"
	"gitlab.com/veroskevin/miku-backend/comment/repository"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetByID(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	query := NewCommentQueryCockroach(db)
	id := uuid.NewV4()
	articleID := uuid.NewV4()
	userID := uuid.NewV4()
	now := time.Now()
	info := repository.PageInfo{Size: 10, Cursor: uuid.NullUUID{}.UUID}

	t.Run("When comment exists", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "body", "user_id", "article_id", "created_at", "updated_at"})
		rows = rows.AddRow(id, "body", userID, articleID, now, now)
		rows = rows.AddRow(id, "haha", userID, articleID, now, now)

		mock.ExpectQuery(`SELECT (.+) FROM comments WHERE article_id = ?`).
			WithArgs(articleID).
			WillReturnRows(rows)

		result := <-query.GetByArticleID(articleID, info)
		assert.Nil(t, result.Error)
		assert.NotEmpty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("When query error", func(t *testing.T) {
		mock.ExpectQuery(`SELECT (.+) FROM comments WHERE article_id = ?`).
			WithArgs(articleID).
			WillReturnError(sql.ErrConnDone)

		result := <-query.GetByArticleID(articleID, info)
		assert.NotNil(t, result.Error)
		assert.Empty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("When scanning rows error", func(t *testing.T) {
		rows := sqlmock.NewRows([]string{"id", "body", "user_id", "article_id", "created_at", "updated_at"})
		rows = rows.AddRow(id, "body", userID, articleID, now, now)
		rows = rows.AddRow(id, "haha", nil, articleID, now, now)

		mock.ExpectQuery(`SELECT (.+) FROM comments WHERE article_id = ?`).
			WithArgs(articleID).
			WillReturnRows(rows)


		result := <-query.GetByArticleID(articleID, info)
		assert.NotNil(t, result.Error)
		assert.Empty(t, result.Result)

		err := mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}
