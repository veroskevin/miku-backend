package cockroach

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/veroskevin/miku-backend/comment/domain"
	"testing"
)

func TestCreate(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()
	repo := NewCommentRepositoryCockroach(db)

	t.Run("Insert success", func(t *testing.T) {
		comment := domain.NewComment("body", uuid.NewV4(), uuid.NewV4())
		mock.ExpectExec(`INSERT INTO comments`).
			WithArgs(
				comment.ID, comment.Body, comment.UserID,
				comment.ArticleID, comment.CreatedAt, comment.UpdatedAt).
			WillReturnResult(sqlmock.NewResult(1, 1))

		err := repo.Create(comment)
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run("Insert fail", func(t *testing.T) {
		comment := domain.NewComment("body", uuid.NewV4(), uuid.NewV4())
		mock.ExpectExec(`INSERT INTO comments`).
			WithArgs(
				comment.ID, comment.Body, comment.UserID,
				comment.ArticleID, comment.CreatedAt, comment.UpdatedAt).
			WillReturnError(sql.ErrConnDone)

		err := repo.Create(comment)
		assert.NotNil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

}
