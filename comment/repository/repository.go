package repository

import (
	"gitlab.com/veroskevin/miku-backend/comment/domain"
)

// CommentRepository interface
type CommentRepository interface {
	Create(*domain.Comment) error
}

type PageInfo struct {
	Size   int32
	Cursor string
}
