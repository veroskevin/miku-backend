CREATE TABLE IF NOT EXISTS comments(
    id UUID PRIMARY KEY,
    body TEXT,
    user_id UUID,
    article_id UUID,
    created_at timestamp,
    updated_at timestamp
);

CREATE INDEX IF NOT EXISTS comment_user_id_idx ON comments(user_id);
CREATE INDEX IF NOT EXISTS comment_article_id_idx ON comments(article_id);