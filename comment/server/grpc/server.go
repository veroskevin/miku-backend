package grpc

import (
	"context"
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/comment/repository"

	pb "gitlab.com/veroskevin/miku-backend/comment/proto"
	"gitlab.com/veroskevin/miku-backend/comment/service"
)

// CommentServer is the implementor of
// CommentServiceServer in proto
type CommentServer struct {
	Service *service.CommentService
}

// NewCommentServer used to a new comment server
func NewCommentServer(service *service.CommentService) *CommentServer {
	return &CommentServer{
		Service: service,
	}
}

// CreateComment used to create a new comment
func (s *CommentServer) CreateComment(ctx context.Context, param *pb.CommentRequest) (*pb.CommentResponse, error) {
	userID, _ := uuid.FromString(param.UserId)
	articleID, _ := uuid.FromString(param.ArticleId)
	comment, err := s.Service.CreateComment(param.Body, articleID, userID)
	if err != nil {
		return nil, err
	}

	return comment.Proto(), nil
}

// GetCommentsByArticleID used to get an array of comment with articleID provided
func (s *CommentServer) GetCommentsByArticleID(ctx context.Context, param *pb.CommentRequest) (*pb.ListCommentResponse, error) {
	var listCommentResponse pb.ListCommentResponse
	articleID, _ := uuid.FromString(param.ArticleId)
	info := repository.PageInfo{Size: param.First, Cursor: param.After}
	comments, err := s.Service.GetCommentsByArticleID(articleID, info)
	if err != nil {
		return nil, err
	}

	for _, v := range comments {
		listCommentResponse.Data = append(listCommentResponse.Data, v.Proto())
	}

	return &listCommentResponse, nil
}

// CountByArticleID return total comments by article id
func (s *CommentServer) CountByArticleID(ctx context.Context, param *pb.CommentRequest) (*pb.CountByArticleIDResponse, error) {
	articleID, _ := uuid.FromString(param.ArticleId)
	count, err := s.Service.CountByArticleID(articleID)
	if err != nil {
		return nil, err
	}
	return &pb.CountByArticleIDResponse{Data: count}, nil
}
