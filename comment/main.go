package main

import (
	"log"
	"net"

	"gitlab.com/veroskevin/miku-backend/comment/config"

	pb "gitlab.com/veroskevin/miku-backend/comment/proto"
	server "gitlab.com/veroskevin/miku-backend/comment/server/grpc"
	"gitlab.com/veroskevin/miku-backend/comment/service"
	"google.golang.org/grpc"
)

func main() {
	port := config.GrpcCommentServicePort
	commentService := service.NewCommentService()
	commentServer := server.NewCommentServer(commentService)

	grpcServer := grpc.NewServer()

	pb.RegisterCommentServiceServer(grpcServer, commentServer)

	log.Println("Starting RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", port, err)
	}

	log.Fatal("Server is listening", grpcServer.Serve(listen))
}
