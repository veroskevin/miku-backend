package service

import (
	"github.com/satori/go.uuid"
	"gitlab.com/veroskevin/miku-backend/comment/config"
	"gitlab.com/veroskevin/miku-backend/comment/domain"
	"gitlab.com/veroskevin/miku-backend/comment/repository"
	"gitlab.com/veroskevin/miku-backend/comment/repository/cockroach"
	"log"
)

// CommentService is service for comment domain
type CommentService struct {
	Query      repository.CommentQuery
	Repository repository.CommentRepository
}

// NewCommentService used to create a new comment service
func NewCommentService() *CommentService {
	db, err := config.InitDB()
	if err != nil {
		log.Fatal(err)
	}
	query := cockroach.NewCommentQueryCockroach(db, config.NewRedis(config.RedisURL, config.RedisDB))
	repository := cockroach.NewCommentRepositoryCockroach(db, config.NewRedis(config.RedisURL, config.RedisDB))

	return &CommentService{
		Query:      query,
		Repository: repository,
	}
}

// CreateComment used to create a new comment
func (s *CommentService) CreateComment(body string, articleID, userID uuid.UUID) (*domain.Comment, error) {
	comment := domain.NewComment(body, articleID, userID)
	err := s.Repository.Create(comment)
	if err != nil {
		return nil, err
	}

	return comment, nil
}

// GetCommentsByArticleID used to get comments with articleID as provided
func (s *CommentService) GetCommentsByArticleID(articleID uuid.UUID, info repository.PageInfo) ([]domain.Comment, error) {
	result := <-s.Query.GetByArticleID(articleID, info)
	if result.Error != nil {
		return nil, result.Error
	}

	comments := result.Result.([]domain.Comment)

	return comments, nil
}

// CountByArticleID return total comments by article id
func (s *CommentService) CountByArticleID(articleID uuid.UUID) (int32, error) {
	result := s.Query.CountByArticleID(articleID)
	if result.Error != nil {
		return 0, result.Error
	}
	return result.Result.(int32), nil
}
