package domain

import (
	"github.com/golang/protobuf/ptypes"
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/veroskevin/miku-backend/comment/proto"
	"time"
)

// Comment is the representation of comment
type Comment struct {
	ID        uuid.UUID
	Body      string
	ArticleID uuid.UUID
	UserID    uuid.UUID
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Proto used to convert comment struct to proto
func (c *Comment) Proto() *pb.CommentResponse {
	createdAt, _ := ptypes.TimestampProto(c.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(c.UpdatedAt)
	return &pb.CommentResponse{
		Id:        c.ID.String(),
		Body:      c.Body,
		ArticleId: c.ArticleID.String(),
		UserId:    c.UserID.String(),
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}
}

// NewComment used to create a new comment
func NewComment(body string, articleID, userID uuid.UUID) *Comment {
	return &Comment{
		ID:        uuid.NewV4(),
		Body:      body,
		ArticleID: articleID,
		UserID:    userID,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}
