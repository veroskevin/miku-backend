package domain

import (
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewComment(t *testing.T) {
	body := "body"
	articleID := uuid.NewV4()
	userID := uuid.NewV4()
	got := NewComment(body, articleID, userID)
	assert.NotNil(t, got)
}

func TestProto(t *testing.T) {
	body := "haha"
	articleID := uuid.NewV4()
	userID := uuid.NewV4()

	comment := NewComment(body, articleID, userID)

	got := comment.Proto()
	assert.NotNil(t, got)
}
