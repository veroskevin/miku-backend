package storage

import (
	"gitlab.com/veroskevin/miku-backend/comment/domain"
)

// Storage is to hold comment data
type Storage struct {
	Comments []domain.Comment
}

// NewStorage used to create a new storage
func NewStorage() *Storage {
	return &Storage{}
}
